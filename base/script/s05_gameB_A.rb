require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s05_gameB_A(s)
  s.label :addr_0x62496
  s.ins MOV, byte(0), ushort(113), 1
  s.ins JUMP, :addr_0x8647
  s.ins DEBUG, 's05_gameB_A01 start.', byte(0)
  s.label :game_05_gameB前半01
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :addr_0x5914, [102, -479, 0, 100]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3324), byte(0), line(s, 3324)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [296, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 296
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(3325), byte(3), line(s, 3325)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x627a2
  s.label :addr_0x625a7
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins MSGSET, uint(3326), byte(0), line(s, 3326)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [102]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [296, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 296
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3327), byte(3), line(s, 3327)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x627a2
  s.label :addr_0x626bf
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :addr_0x5914, [102, -479, 0, 100]
  s.ins MSGSET, uint(3328), byte(0), line(s, 3328)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [296, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 296
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(3329), byte(3), line(s, 3329)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x627a2
  s.label :addr_0x627a2
  s.ins MSGSET, uint(3330), byte(3), line(s, 3330)
  s.ins MSGSYNC, byte(0), 2581
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [277, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 277
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [130, Register.new(213)]
  s.ins MOV, byte(0), ushort(213), 130
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [130, Register.new(214)]
  s.ins MOV, byte(0), ushort(214), 130
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(3331), byte(0), line(s, 3331)
  s.ins MSGSYNC, byte(0), 2059
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSYNC, byte(0), 3344
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3332), byte(0), line(s, 3332)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(213)]
  s.ins MOV, byte(0), ushort(213), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(214)]
  s.ins MOV, byte(0), ushort(214), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 20, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3333), byte(3), line(s, 3333)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3334), byte(2), line(s, 3334)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3335), byte(3), line(s, 3335)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3336), byte(3), line(s, 3336)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3337), byte(3), line(s, 3337)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3338), byte(3), line(s, 3338)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3339), byte(4), line(s, 3339)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3340), byte(3), line(s, 3340)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 33, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3341), byte(0), line(s, 3341)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3342), byte(3), line(s, 3342)
  s.ins MSGSYNC, byte(0), 3989
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 133, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 6336
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3343), byte(3), line(s, 3343)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 24, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3344), byte(2), line(s, 3344)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3345), byte(3), line(s, 3345)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3346), byte(3), line(s, 3346)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3347), byte(2), line(s, 3347)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3348), byte(1), line(s, 3348)
  s.ins MSGSYNC, byte(0), 3317
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6ccf, [45, 500, 250]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGSET, uint(3349), byte(1), line(s, 3349)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 114, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3350), byte(3), line(s, 3350)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [2, 2, 1]
  s.ins MSGSET, uint(3351), byte(2), line(s, 3351)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x527a, []
  s.ins MSGSET, uint(3352), byte(2), line(s, 3352)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3353), byte(2), line(s, 3353)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3354), byte(0), line(s, 3354)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3355), byte(2), line(s, 3355)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3356), byte(2), line(s, 3356)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [99]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3357), byte(0), line(s, 3357)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [108]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(3358), byte(0), line(s, 3358)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3359), byte(0), line(s, 3359)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(3360), byte(0), line(s, 3360)
  s.ins MSGSYNC, byte(0), 1344
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 1920
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2304
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSYNC, byte(0), 3082
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 3562
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 4192
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 5216
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3361), byte(2), line(s, 3361)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3362), byte(2), line(s, 3362)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [570, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 570
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3363), byte(2), line(s, 3363)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [131]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(3364), byte(0), line(s, 3364)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [4200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1400, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3365), byte(127), line(s, 3365)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3366), byte(3), line(s, 3366)
  s.ins MSGSYNC, byte(0), 3733
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3367), byte(3), line(s, 3367)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [114]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3368), byte(0), line(s, 3368)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3369), byte(3), line(s, 3369)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3370), byte(3), line(s, 3370)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3371), byte(0), line(s, 3371)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3372), byte(0), line(s, 3372)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3373), byte(127), line(s, 3373)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3374), byte(127), line(s, 3374)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [527, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 527
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3375), byte(127), line(s, 3375)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 137, 1, 0, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 12288, 0]
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 240, 1, 0, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 12288, 0]
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3376), byte(1), line(s, 3376)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3377), byte(2), line(s, 3377)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 500
  s.ins MOV, byte(0), ushort(65), 50
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins MSGSET, uint(3378), byte(3), line(s, 3378)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [109]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3379), byte(0), line(s, 3379)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3380), byte(0), line(s, 3380)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [471]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [35, 35, 1]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins MSGSET, uint(3381), byte(0), line(s, 3381)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3382), byte(127), line(s, 3382)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3383), byte(127), line(s, 3383)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3384), byte(127), line(s, 3384)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 70, 3000]
  s.ins MSGSET, uint(3385), byte(0), line(s, 3385)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3386), byte(127), line(s, 3386)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3387), byte(127), line(s, 3387)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3388), byte(127), line(s, 3388)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3389), byte(127), line(s, 3389)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3390), byte(2), line(s, 3390)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3391), byte(0), line(s, 3391)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3392), byte(3), line(s, 3392)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins CALL, :f_se_fadeout, [3, 100]
  s.ins CALL, :f_se_play, [0, 54, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 53, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [16, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-499, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 29, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3393), byte(1), line(s, 3393)
  s.ins MSGSYNC, byte(0), 949
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 67, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 30, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MSGSET, uint(3394), byte(3), line(s, 3394)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 139, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MSGSET, uint(3395), byte(2), line(s, 3395)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3396), byte(1), line(s, 3396)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3397), byte(127), line(s, 3397)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3398), byte(127), line(s, 3398)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3399), byte(127), line(s, 3399)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [128, 0, 0, 100]
  s.ins MSGSET, uint(3400), byte(127), line(s, 3400)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3401), byte(127), line(s, 3401)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3402), byte(127), line(s, 3402)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3403), byte(0), line(s, 3403)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3404), byte(3), line(s, 3404)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3405), byte(0), line(s, 3405)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3406), byte(3), line(s, 3406)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3407), byte(0), line(s, 3407)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3408), byte(0), line(s, 3408)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3409), byte(3), line(s, 3409)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3410), byte(0), line(s, 3410)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3411), byte(0), line(s, 3411)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3412), byte(3), line(s, 3412)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3413), byte(3), line(s, 3413)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3414), byte(3), line(s, 3414)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [285, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 285
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(3415), byte(3), line(s, 3415)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3416), byte(0), line(s, 3416)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [119]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3417), byte(0), line(s, 3417)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [18, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3418), byte(127), line(s, 3418)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3419), byte(127), line(s, 3419)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3420), byte(127), line(s, 3420)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 70, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [121]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [105, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3421), byte(0), line(s, 3421)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3422), byte(0), line(s, 3422)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3423), byte(0), line(s, 3423)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3424), byte(0), line(s, 3424)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3425), byte(0), line(s, 3425)
  s.ins MSGSYNC, byte(0), 2133
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [111]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [105, 0, 0, 0]
  s.ins MSGSYNC, byte(0), 2245
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 400]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-499, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-499, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 400, 12291, 0]
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x65354
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x65354
  s.ins MOV, byte(0), ushort(114), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A02 start.', byte(0)
  s.label :game_05_gameB前半02
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6547c
  s.label :addr_0x653ce
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6547c
  s.label :addr_0x65425
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6547c
  s.label :addr_0x6547c
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3426), byte(127), line(s, 3426)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 131, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3427), byte(3), line(s, 3427)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [528, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 528
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 20, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3428), byte(3), line(s, 3428)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3429), byte(3), line(s, 3429)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 40, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3430), byte(1), line(s, 3430)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3431), byte(2), line(s, 3431)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3432), byte(3), line(s, 3432)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3433), byte(3), line(s, 3433)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [127]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3434), byte(0), line(s, 3434)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3435), byte(0), line(s, 3435)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3436), byte(1), line(s, 3436)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3437), byte(1), line(s, 3437)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3438), byte(3), line(s, 3438)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3439), byte(1), line(s, 3439)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [105, 0, 0, 100]
  s.ins MSGSET, uint(3440), byte(0), line(s, 3440)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3441), byte(1), line(s, 3441)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MSGSET, uint(3442), byte(1), line(s, 3442)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins MSGSET, uint(3443), byte(0), line(s, 3443)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 11, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3444), byte(1), line(s, 3444)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3445), byte(1), line(s, 3445)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3446), byte(3), line(s, 3446)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3447), byte(3), line(s, 3447)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3448), byte(1), line(s, 3448)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3449), byte(1), line(s, 3449)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3450), byte(1), line(s, 3450)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3451), byte(1), line(s, 3451)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 6, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3452), byte(0), line(s, 3452)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3453), byte(3), line(s, 3453)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3454), byte(3), line(s, 3454)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3455), byte(2), line(s, 3455)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3456), byte(1), line(s, 3456)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3457), byte(1), line(s, 3457)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3458), byte(2), line(s, 3458)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3459), byte(3), line(s, 3459)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3460), byte(2), line(s, 3460)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3461), byte(2), line(s, 3461)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3462), byte(3), line(s, 3462)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3463), byte(3), line(s, 3463)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 110, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3464), byte(2), line(s, 3464)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 139, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3465), byte(1), line(s, 3465)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3466), byte(0), line(s, 3466)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3467), byte(3), line(s, 3467)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 14, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3468), byte(3), line(s, 3468)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3469), byte(3), line(s, 3469)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3470), byte(3), line(s, 3470)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 129, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3471), byte(3), line(s, 3471)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3472), byte(127), line(s, 3472)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -199
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 128, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MSGSET, uint(3473), byte(1), line(s, 3473)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MSGSET, uint(3474), byte(2), line(s, 3474)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3475), byte(3), line(s, 3475)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3476), byte(1), line(s, 3476)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3477), byte(3), line(s, 3477)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3478), byte(2), line(s, 3478)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSET, uint(3479), byte(1), line(s, 3479)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3480), byte(1), line(s, 3480)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3481), byte(3), line(s, 3481)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3482), byte(3), line(s, 3482)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3483), byte(3), line(s, 3483)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 220, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3484), byte(1), line(s, 3484)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3485), byte(3), line(s, 3485)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3486), byte(3), line(s, 3486)
  s.ins MSGSYNC, byte(0), 1930
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 2677
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [220, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, :null, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, :null, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3487), byte(127), line(s, 3487)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3488), byte(127), line(s, 3488)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3489), byte(127), line(s, 3489)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3490), byte(3), line(s, 3490)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_se_play, [0, 79, 100, 0, 0]
  s.ins MSGSET, uint(3491), byte(127), line(s, 3491)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3492), byte(127), line(s, 3492)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 79, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3493), byte(1), line(s, 3493)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [571, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 571
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3494), byte(3), line(s, 3494)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [602, Register.new(191)]
  s.ins MOV, byte(0), ushort(191), 602
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [0, 33, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins LAYERCTRL, 32, 32, byte(5), 6, 256
  s.ins LAYERCTRL, 32, 33, byte(1), 20
  s.ins LAYERCTRL, 32, 34, byte(1), 3368
  s.ins LAYERCTRL, 32, 35, byte(1), -1683
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 200, 3, 0]
  s.ins CALL, :addr_0x4df4, [3100]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3495), byte(127), line(s, 3495)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3496), byte(127), line(s, 3496)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3497), byte(127), line(s, 3497)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3498), byte(127), line(s, 3498)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3499), byte(127), line(s, 3499)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 44, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [472]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(13), 7
  s.ins MOV, byte(0), ushort(14), 501
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(3500), byte(2), line(s, 3500)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3501), byte(1), line(s, 3501)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 34, 100, 0, 0]
  s.ins MSGSET, uint(3502), byte(127), line(s, 3502)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3503), byte(127), line(s, 3503)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3504), byte(127), line(s, 3504)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3505), byte(3), line(s, 3505)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3506), byte(3), line(s, 3506)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3507), byte(0), line(s, 3507)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3508), byte(3), line(s, 3508)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3509), byte(3), line(s, 3509)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3510), byte(0), line(s, 3510)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSET, uint(3511), byte(3), line(s, 3511)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3512), byte(3), line(s, 3512)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 24, 100, 0, 0]
  s.ins MSGSET, uint(3513), byte(127), line(s, 3513)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 138, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3514), byte(3), line(s, 3514)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 123, 100, 0, 0]
  s.ins MSGSET, uint(3515), byte(127), line(s, 3515)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 16, 100, 0, 0]
  s.ins MSGSET, uint(3516), byte(127), line(s, 3516)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3517), byte(3), line(s, 3517)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGSET, uint(3518), byte(127), line(s, 3518)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 21, 100, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [Register.new(191), Register.new(191)]
  s.ins MOV, byte(0), ushort(191), Register.new(191)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ce, [0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(3519), byte(127), line(s, 3519)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3520), byte(3), line(s, 3520)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3521), byte(3), line(s, 3521)
  s.ins MSGSYNC, byte(0), 4010
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 130, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3522), byte(0), line(s, 3522)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3523), byte(3), line(s, 3523)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3524), byte(3), line(s, 3524)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3525), byte(0), line(s, 3525)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3526), byte(0), line(s, 3526)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3527), byte(3), line(s, 3527)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 133, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3528), byte(3), line(s, 3528)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3529), byte(0), line(s, 3529)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3530), byte(127), line(s, 3530)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3531), byte(127), line(s, 3531)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-3399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3532), byte(0), line(s, 3532)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3533), byte(3), line(s, 3533)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3534), byte(3), line(s, 3534)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3535), byte(0), line(s, 3535)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-50, 300, 133, 1]
  s.ins MSGSET, uint(3536), byte(127), line(s, 3536)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [221]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [280, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [175, 0, 0, 0]
  s.ins LAYERCTRL, -4, 24, byte(1), 4
  s.ins LAYERCTRL, -4, 28, byte(1), 1000
  s.ins LAYERCTRL, -4, 29, byte(1), 1000
  s.ins LAYERCTRL, -4, 30, byte(1), 1000
  s.ins LAYERCTRL, -4, 31, byte(1), 1000
  s.ins CALL, :f_se_play, [0, 58, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins MSGSET, uint(3537), byte(127), line(s, 3537)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3538), byte(127), line(s, 3538)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :f_se_play, [0, 67, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [16, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-3399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERCTRL, -4, 24, byte(0)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3539), byte(0), line(s, 3539)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [1, 54, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3540), byte(3), line(s, 3540)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 53, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3541), byte(0), line(s, 3541)
  s.ins MSGSYNC, byte(0), 3808
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [17, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3542), byte(127), line(s, 3542)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(13), 47
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [18, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [80, 200, 0, 0]
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3543), byte(127), line(s, 3543)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [1, 79, 100, 0, 0]
  s.ins MSGSET, uint(3544), byte(127), line(s, 3544)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 70, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [17, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 104, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 122, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3545), byte(3), line(s, 3545)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3546), byte(0), line(s, 3546)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3547), byte(3), line(s, 3547)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3548), byte(0), line(s, 3548)
  s.ins MSGSYNC, byte(0), 5354
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3549), byte(3), line(s, 3549)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x690cf
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x690cf
  s.ins MOV, byte(0), ushort(115), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A03 start.', byte(0)
  s.label :game_05_gameB前半03
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3550), byte(2), line(s, 3550)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 6, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3551), byte(2), line(s, 3551)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x6959a
  s.label :addr_0x69258
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(3552), byte(2), line(s, 3552)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 6, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3553), byte(2), line(s, 3553)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x6959a
  s.label :addr_0x693f9
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins MSGSET, uint(3554), byte(2), line(s, 3554)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 6, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3555), byte(2), line(s, 3555)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP, :addr_0x6959a
  s.label :addr_0x6959a
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3556), byte(2), line(s, 3556)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3557), byte(2), line(s, 3557)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3558), byte(2), line(s, 3558)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 127, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 18, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3559), byte(1), line(s, 3559)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3560), byte(0), line(s, 3560)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :f_se_play, [0, 20, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3561), byte(127), line(s, 3561)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3562), byte(0), line(s, 3562)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3563), byte(4), line(s, 3563)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3564), byte(0), line(s, 3564)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 236, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3565), byte(2), line(s, 3565)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3566), byte(2), line(s, 3566)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3567), byte(1), line(s, 3567)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3568), byte(2), line(s, 3568)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3569), byte(2), line(s, 3569)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 131, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3570), byte(3), line(s, 3570)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3571), byte(2), line(s, 3571)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [572, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 572
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3572), byte(2), line(s, 3572)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x5c62, [1]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3573), byte(127), line(s, 3573)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3574), byte(127), line(s, 3574)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 18, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 500]
  s.ins MSGSET, uint(3575), byte(127), line(s, 3575)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3576), byte(127), line(s, 3576)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 17, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [2, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3577), byte(1), line(s, 3577)
  s.ins MSGSYNC, byte(0), 1309
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3578), byte(0), line(s, 3578)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3579), byte(2), line(s, 3579)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3580), byte(1), line(s, 3580)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3581), byte(0), line(s, 3581)
  s.ins MSGSYNC, byte(0), 1717
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3582), byte(1), line(s, 3582)
  s.ins MSGSYNC, byte(0), 1560
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 200]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSET, uint(3583), byte(1), line(s, 3583)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3584), byte(1), line(s, 3584)
  s.ins MSGSYNC, byte(0), 3920
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3585), byte(2), line(s, 3585)
  s.ins MSGSYNC, byte(0), 2789
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 5280
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 5, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGSET, uint(3586), byte(127), line(s, 3586)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3587), byte(127), line(s, 3587)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [243]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3588), byte(3), line(s, 3588)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3589), byte(1), line(s, 3589)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3590), byte(2), line(s, 3590)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [254]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3591), byte(3), line(s, 3591)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3592), byte(3), line(s, 3592)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3593), byte(3), line(s, 3593)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3594), byte(3), line(s, 3594)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3595), byte(3), line(s, 3595)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3596), byte(3), line(s, 3596)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 1, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3597), byte(2), line(s, 3597)
  s.ins MSGSYNC, byte(0), 2634
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3598), byte(2), line(s, 3598)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3599), byte(2), line(s, 3599)
  s.ins MSGSYNC, byte(0), 2346
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 4448
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 126, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3600), byte(2), line(s, 3600)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 139, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3601), byte(1), line(s, 3601)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3602), byte(1), line(s, 3602)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [254]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3603), byte(3), line(s, 3603)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3604), byte(3), line(s, 3604)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3605), byte(2), line(s, 3605)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins MSGSET, uint(3606), byte(127), line(s, 3606)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3607), byte(127), line(s, 3607)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3608), byte(127), line(s, 3608)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 37, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3609), byte(2), line(s, 3609)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3610), byte(1), line(s, 3610)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3611), byte(2), line(s, 3611)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3612), byte(1), line(s, 3612)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 126, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3613), byte(0), line(s, 3613)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [254]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3614), byte(3), line(s, 3614)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3615), byte(3), line(s, 3615)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3616), byte(3), line(s, 3616)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3617), byte(3), line(s, 3617)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [245, 0, 0, 100]
  s.ins MSGSET, uint(3618), byte(3), line(s, 3618)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x6b48a
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x6b48a
  s.ins MOV, byte(0), ushort(116), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A04 start.', byte(0)
  s.label :game_05_gameB前半04
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [104]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6b618
  s.label :addr_0x6b526
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [104]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6b618
  s.label :addr_0x6b59f
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [104]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6b618
  s.label :addr_0x6b618
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3619), byte(0), line(s, 3619)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3620), byte(0), line(s, 3620)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3621), byte(3), line(s, 3621)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 130, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3622), byte(3), line(s, 3622)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3623), byte(3), line(s, 3623)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 28, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3624), byte(1), line(s, 3624)
  s.ins MSGSYNC, byte(0), 938
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3625), byte(1), line(s, 3625)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [124]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3626), byte(0), line(s, 3626)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3627), byte(0), line(s, 3627)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3628), byte(0), line(s, 3628)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3629), byte(1), line(s, 3629)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3630), byte(0), line(s, 3630)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3631), byte(0), line(s, 3631)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3632), byte(1), line(s, 3632)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 25, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 111, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3633), byte(2), line(s, 3633)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3634), byte(2), line(s, 3634)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3635), byte(3), line(s, 3635)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3636), byte(3), line(s, 3636)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3637), byte(2), line(s, 3637)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3638), byte(2), line(s, 3638)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3639), byte(3), line(s, 3639)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [124]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3640), byte(0), line(s, 3640)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3641), byte(0), line(s, 3641)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3642), byte(2), line(s, 3642)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 19, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [2000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3643), byte(1), line(s, 3643)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3644), byte(1), line(s, 3644)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3645), byte(1), line(s, 3645)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_se_play, [0, 28, 100, 0, 0]
  s.ins MSGSET, uint(3646), byte(127), line(s, 3646)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3647), byte(127), line(s, 3647)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [125]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3648), byte(0), line(s, 3648)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3649), byte(0), line(s, 3649)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3650), byte(0), line(s, 3650)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3651), byte(1), line(s, 3651)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3652), byte(3), line(s, 3652)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3653), byte(3), line(s, 3653)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3654), byte(0), line(s, 3654)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3655), byte(2), line(s, 3655)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3656), byte(1), line(s, 3656)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3657), byte(0), line(s, 3657)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3658), byte(1), line(s, 3658)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3659), byte(1), line(s, 3659)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3660), byte(1), line(s, 3660)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 25, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3661), byte(2), line(s, 3661)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3662), byte(1), line(s, 3662)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3663), byte(1), line(s, 3663)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 34, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins MSGSET, uint(3664), byte(2), line(s, 3664)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3665), byte(2), line(s, 3665)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 40, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3666), byte(1), line(s, 3666)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3667), byte(2), line(s, 3667)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3668), byte(2), line(s, 3668)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3669), byte(2), line(s, 3669)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3670), byte(2), line(s, 3670)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3671), byte(2), line(s, 3671)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3672), byte(2), line(s, 3672)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 20, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3673), byte(0), line(s, 3673)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3674), byte(2), line(s, 3674)
  s.ins MSGSYNC, byte(0), 5141
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 15, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3675), byte(3), line(s, 3675)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3676), byte(3), line(s, 3676)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3677), byte(2), line(s, 3677)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3678), byte(2), line(s, 3678)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3679), byte(2), line(s, 3679)
  s.ins MSGSYNC, byte(0), 5653
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3680), byte(2), line(s, 3680)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 139, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3681), byte(1), line(s, 3681)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3682), byte(2), line(s, 3682)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [125]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3683), byte(0), line(s, 3683)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3684), byte(0), line(s, 3684)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [132, 0, 0, 100]
  s.ins MSGSET, uint(3685), byte(0), line(s, 3685)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [570, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 570
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3686), byte(2), line(s, 3686)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [128]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3687), byte(0), line(s, 3687)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3688), byte(2), line(s, 3688)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3689), byte(0), line(s, 3689)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3690), byte(0), line(s, 3690)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [132, Register.new(213)]
  s.ins MOV, byte(0), ushort(213), 132
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [132, Register.new(214)]
  s.ins MOV, byte(0), ushort(214), 132
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [125, 125, 500]
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 500]
  s.ins MSGSET, uint(3691), byte(0), line(s, 3691)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 54, 54
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(213)]
  s.ins MOV, byte(0), ushort(213), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 55, 55
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(214)]
  s.ins MOV, byte(0), ushort(214), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 129, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3692), byte(1), line(s, 3692)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3693), byte(1), line(s, 3693)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 209, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3694), byte(2), line(s, 3694)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 217, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3695), byte(2), line(s, 3695)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3696), byte(1), line(s, 3696)
  s.ins MSGSYNC, byte(0), 2133
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3697), byte(1), line(s, 3697)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3698), byte(1), line(s, 3698)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3699), byte(3), line(s, 3699)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3700), byte(2), line(s, 3700)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3701), byte(1), line(s, 3701)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3702), byte(1), line(s, 3702)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3703), byte(1), line(s, 3703)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3704), byte(1), line(s, 3704)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 18, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3705), byte(0), line(s, 3705)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3706), byte(1), line(s, 3706)
  s.ins MSGSYNC, byte(0), 4693
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3707), byte(1), line(s, 3707)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3708), byte(1), line(s, 3708)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3709), byte(3), line(s, 3709)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSET, uint(3710), byte(1), line(s, 3710)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3711), byte(1), line(s, 3711)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3712), byte(1), line(s, 3712)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3713), byte(1), line(s, 3713)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3714), byte(1), line(s, 3714)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3715), byte(1), line(s, 3715)
  s.ins MSGSYNC, byte(0), 3968
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3716), byte(1), line(s, 3716)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3717), byte(0), line(s, 3717)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3718), byte(1), line(s, 3718)
  s.ins MSGSYNC, byte(0), 3776
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 5397
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3719), byte(1), line(s, 3719)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3720), byte(2), line(s, 3720)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [194]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3721), byte(1), line(s, 3721)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3722), byte(127), line(s, 3722)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3723), byte(1), line(s, 3723)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3724), byte(3), line(s, 3724)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3725), byte(1), line(s, 3725)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3726), byte(3), line(s, 3726)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3727), byte(1), line(s, 3727)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x5914, [195, 0, 0, 100]
  s.ins MSGSET, uint(3728), byte(3), line(s, 3728)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [184, 0, 0, 100]
  s.ins MSGSET, uint(3729), byte(1), line(s, 3729)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [12, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 32, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3730), byte(3), line(s, 3730)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 230, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSET, uint(3731), byte(2), line(s, 3731)
  s.ins MSGSYNC, byte(0), 2837
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 233, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [186]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3732), byte(1), line(s, 3732)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3733), byte(3), line(s, 3733)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [199, 0, 0, 100]
  s.ins MSGSET, uint(3734), byte(1), line(s, 3734)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3735), byte(1), line(s, 3735)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [800, 0, 0, 0]
  s.ins PLANESELECT, byte(2)
  s.ins MASKLOAD, 77, 0, 0
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 508
  s.ins PLANESELECT, byte(1)
  s.ins MASKLOAD, 76, 0, 0
  s.ins LAYERSELECT, 38, 38
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [100, Register.new(197)]
  s.ins MOV, byte(0), ushort(197), 100
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-599, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins PLANESELECT, byte(0)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3736), byte(0), line(s, 3736)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3737), byte(0), line(s, 3737)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3738), byte(0), line(s, 3738)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3739), byte(1), line(s, 3739)
  s.ins MSGWAIT, byte(127)
  s.ins PLANESELECT, byte(1)
  s.ins LAYERSELECT, 38, 38
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [137, Register.new(197)]
  s.ins MOV, byte(0), ushort(197), 137
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-599, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins PLANESELECT, byte(0)
  s.ins MSGSET, uint(3740), byte(0), line(s, 3740)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3741), byte(0), line(s, 3741)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3742), byte(1), line(s, 3742)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 57, 57
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(216)]
  s.ins MOV, byte(0), ushort(216), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 38, 38
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(197)]
  s.ins MOV, byte(0), ushort(197), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins PLANESELECT, byte(2)
  s.ins PLANECLEAR
  s.ins LAYERINIT, -4
  s.ins PLANESELECT, byte(1)
  s.ins PLANECLEAR
  s.ins PLANESELECT, byte(0)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [474]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(3743), byte(127), line(s, 3743)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3744), byte(127), line(s, 3744)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3745), byte(0), line(s, 3745)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3746), byte(127), line(s, 3746)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [106]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3747), byte(0), line(s, 3747)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3748), byte(0), line(s, 3748)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [94, 0, 0, 100]
  s.ins MSGSET, uint(3749), byte(0), line(s, 3749)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3750), byte(127), line(s, 3750)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x6f48f
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x6f48f
  s.ins MOV, byte(0), ushort(117), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A05 start.', byte(0)
  s.label :game_05_gameB前半05
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(181)]
  s.ins MOV, byte(0), ushort(181), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 129, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6f81a
  s.label :addr_0x6f5f2
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 129, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6f81a
  s.label :addr_0x6f706
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 129, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins JUMP, :addr_0x6f81a
  s.label :addr_0x6f81a
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3751), byte(3), line(s, 3751)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3752), byte(3), line(s, 3752)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 112, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3753), byte(2), line(s, 3753)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3754), byte(3), line(s, 3754)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3755), byte(3), line(s, 3755)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3756), byte(3), line(s, 3756)
  s.ins MSGSYNC, byte(0), 4330
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 27, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [800]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [300, 0, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [602, Register.new(191)]
  s.ins MOV, byte(0), ushort(191), 602
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERCTRL, 32, 32, byte(5), 6, 256
  s.ins LAYERCTRL, 32, 33, byte(1), 20
  s.ins LAYERCTRL, 32, 34, byte(1), 3368
  s.ins LAYERCTRL, 32, 35, byte(1), -1683
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 200, 3, 0]
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins MSGSET, uint(3757), byte(127), line(s, 3757)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 25, 100, 0, 0]
  s.ins CALL, :addr_0x5892, [232, 0, 0, 100]
  s.ins MSGSET, uint(3758), byte(127), line(s, 3758)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 19, 100, 0, 0]
  s.ins MSGSET, uint(3759), byte(127), line(s, 3759)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 23, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3760), byte(1), line(s, 3760)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3761), byte(1), line(s, 3761)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3762), byte(2), line(s, 3762)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [110, 0, 0, 0]
  s.ins MSGSET, uint(3763), byte(127), line(s, 3763)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3764), byte(1), line(s, 3764)
  s.ins MSGSYNC, byte(0), 1408
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 2000]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 3000]
  s.ins MSGSET, uint(3765), byte(1), line(s, 3765)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 74, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 223, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3766), byte(2), line(s, 3766)
  s.ins MSGSYNC, byte(0), 2485
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 100, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3767), byte(1), line(s, 3767)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-6499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [220, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -199
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins MSGSET, uint(3768), byte(127), line(s, 3768)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3769), byte(127), line(s, 3769)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3770), byte(1), line(s, 3770)
  s.ins MSGSYNC, byte(0), 2837
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [10, 300, 131, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 3000]
  s.ins MSGSYNC, byte(0), 5312
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 53, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [14, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-3499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [120, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3771), byte(2), line(s, 3771)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 105, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3772), byte(3), line(s, 3772)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [1, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3773), byte(1), line(s, 3773)
  s.ins MSGSYNC, byte(0), 2698
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3774), byte(3), line(s, 3774)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 500, 5, -1]
  s.ins MSGSET, uint(3775), byte(2), line(s, 3775)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3776), byte(0), line(s, 3776)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3777), byte(1), line(s, 3777)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3778), byte(3), line(s, 3778)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3779), byte(1), line(s, 3779)
  s.ins MSGSYNC, byte(0), 4053
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3780), byte(3), line(s, 3780)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3781), byte(1), line(s, 3781)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3782), byte(3), line(s, 3782)
  s.ins MSGSYNC, byte(0), 4800
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 5674
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3783), byte(3), line(s, 3783)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [475]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_play, [0, 76, 100, 0, 0]
  s.ins CALL, :f_se_play, [1, 74, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 500]
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(3784), byte(127), line(s, 3784)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :f_se_play, [0, 83, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(3785), byte(127), line(s, 3785)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3786), byte(0), line(s, 3786)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3787), byte(1), line(s, 3787)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 40, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3788), byte(2), line(s, 3788)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3789), byte(1), line(s, 3789)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3790), byte(1), line(s, 3790)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 14, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3791), byte(0), line(s, 3791)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3792), byte(2), line(s, 3792)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3793), byte(1), line(s, 3793)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3794), byte(1), line(s, 3794)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3795), byte(1), line(s, 3795)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3796), byte(1), line(s, 3796)
  s.ins MSGSYNC, byte(0), 3690
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [12, 500, 5, 250]
  s.ins MSGSYNC, byte(0), 6016
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3797), byte(2), line(s, 3797)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3798), byte(1), line(s, 3798)
  s.ins MSGSYNC, byte(0), 2869
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3799), byte(1), line(s, 3799)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3800), byte(2), line(s, 3800)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3801), byte(0), line(s, 3801)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3802), byte(1), line(s, 3802)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3803), byte(1), line(s, 3803)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3804), byte(1), line(s, 3804)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3805), byte(1), line(s, 3805)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3806), byte(2), line(s, 3806)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [358, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 358
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-392, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(3807), byte(1), line(s, 3807)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3808), byte(1), line(s, 3808)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3809), byte(2), line(s, 3809)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3810), byte(127), line(s, 3810)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3811), byte(127), line(s, 3811)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3812), byte(127), line(s, 3812)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3813), byte(1), line(s, 3813)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3814), byte(1), line(s, 3814)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3815), byte(1), line(s, 3815)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3816), byte(0), line(s, 3816)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3817), byte(1), line(s, 3817)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3818), byte(1), line(s, 3818)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3819), byte(1), line(s, 3819)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3820), byte(1), line(s, 3820)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3821), byte(1), line(s, 3821)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3822), byte(0), line(s, 3822)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3823), byte(0), line(s, 3823)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(3824), byte(0), line(s, 3824)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3825), byte(1), line(s, 3825)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3826), byte(1), line(s, 3826)
  s.ins MSGSYNC, byte(0), 3349
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_foreground_picture, [360, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 360
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3827), byte(1), line(s, 3827)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 77, 100, 0, 0]
  s.ins CALL, :addr_0x5388, [35, 35, 250]
  s.ins MSGSET, uint(3828), byte(127), line(s, 3828)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3829), byte(127), line(s, 3829)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [353, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 353
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-99, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3830), byte(1), line(s, 3830)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3831), byte(1), line(s, 3831)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3832), byte(1), line(s, 3832)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3833), byte(1), line(s, 3833)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3834), byte(1), line(s, 3834)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3835), byte(0), line(s, 3835)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [364, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 364
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3836), byte(1), line(s, 3836)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3837), byte(1), line(s, 3837)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 5, 5
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [4, 0, 1, 34, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERCTRL, -6, 22, byte(0)
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3838), byte(0), line(s, 3838)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3839), byte(0), line(s, 3839)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3840), byte(0), line(s, 3840)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 5, 5
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 5, 5
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins CALL, :addr_0x5388, [35, 35, 500]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(3841), byte(0), line(s, 3841)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 5, 5
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [4, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3842), byte(0), line(s, 3842)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3843), byte(0), line(s, 3843)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3844), byte(0), line(s, 3844)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x4f13, [100]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3845), byte(1), line(s, 3845)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3846), byte(1), line(s, 3846)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3847), byte(1), line(s, 3847)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3848), byte(1), line(s, 3848)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3849), byte(1), line(s, 3849)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 41, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3850), byte(2), line(s, 3850)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [528, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 528
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 38, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3851), byte(0), line(s, 3851)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3852), byte(1), line(s, 3852)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 19, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-30, 300, 128, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6db0, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-30, 300, 128, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6db0, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-30, 300, 128, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6db0, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-30, 300, 128, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6db0, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-30, 300, 128, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6db0, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins MSGSET, uint(3853), byte(127), line(s, 3853)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3854), byte(127), line(s, 3854)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3855), byte(1), line(s, 3855)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x7315d
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x7315d
  s.ins MOV, byte(0), ushort(118), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A06 start.', byte(0)
  s.label :game_05_gameB前半06
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(181)]
  s.ins MOV, byte(0), ushort(181), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x73515
  s.label :addr_0x732e5
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x73515
  s.label :addr_0x733fd
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x73515
  s.label :addr_0x73515
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(3856), byte(0), line(s, 3856)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3857), byte(0), line(s, 3857)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3858), byte(0), line(s, 3858)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3859), byte(0), line(s, 3859)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [172]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3860), byte(1), line(s, 3860)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3861), byte(1), line(s, 3861)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3862), byte(1), line(s, 3862)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3863), byte(1), line(s, 3863)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3864), byte(1), line(s, 3864)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [411, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 411
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3865), byte(0), line(s, 3865)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3866), byte(0), line(s, 3866)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3867), byte(0), line(s, 3867)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3868), byte(1), line(s, 3868)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 121, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 25, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3869), byte(3), line(s, 3869)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 212, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3870), byte(2), line(s, 3870)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3871), byte(2), line(s, 3871)
  s.ins MSGSYNC, byte(0), 2112
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSYNC, byte(0), 3072
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3872), byte(2), line(s, 3872)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3873), byte(0), line(s, 3873)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3874), byte(2), line(s, 3874)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3875), byte(0), line(s, 3875)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3876), byte(0), line(s, 3876)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3877), byte(0), line(s, 3877)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 29, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3878), byte(3), line(s, 3878)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3879), byte(0), line(s, 3879)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3880), byte(0), line(s, 3880)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 34, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3881), byte(2), line(s, 3881)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3882), byte(3), line(s, 3882)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3883), byte(3), line(s, 3883)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3884), byte(0), line(s, 3884)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 232, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3885), byte(2), line(s, 3885)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3886), byte(3), line(s, 3886)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3887), byte(0), line(s, 3887)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3888), byte(0), line(s, 3888)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3889), byte(0), line(s, 3889)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 13, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3890), byte(2), line(s, 3890)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3891), byte(2), line(s, 3891)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3892), byte(0), line(s, 3892)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3893), byte(0), line(s, 3893)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 12, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3894), byte(3), line(s, 3894)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3895), byte(0), line(s, 3895)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3896), byte(0), line(s, 3896)
  s.ins MSGSYNC, byte(0), 3072
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3897), byte(0), line(s, 3897)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3898), byte(2), line(s, 3898)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3899), byte(3), line(s, 3899)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3900), byte(3), line(s, 3900)
  s.ins MSGSYNC, byte(0), 5461
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3901), byte(0), line(s, 3901)
  s.ins MSGSYNC, byte(0), 4480
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3902), byte(2), line(s, 3902)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3903), byte(0), line(s, 3903)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3904), byte(0), line(s, 3904)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3905), byte(0), line(s, 3905)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3906), byte(3), line(s, 3906)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [499, Register.new(243), 1400]
  s.ins MOV, byte(0), ushort(243), 499
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x604e, [1300]
  s.ins MSGSET, uint(3907), byte(127), line(s, 3907)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3908), byte(127), line(s, 3908)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(243)]
  s.ins MOV, byte(0), ushort(243), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x60a7, []
  s.ins MSGSET, uint(3909), byte(0), line(s, 3909)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3910), byte(0), line(s, 3910)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [151]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3911), byte(1), line(s, 3911)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins MSGSET, uint(3912), byte(0), line(s, 3912)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3913), byte(2), line(s, 3913)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3914), byte(2), line(s, 3914)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [170, 0, 0, 100]
  s.ins MSGSET, uint(3915), byte(1), line(s, 3915)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3916), byte(1), line(s, 3916)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3917), byte(1), line(s, 3917)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3918), byte(1), line(s, 3918)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3919), byte(3), line(s, 3919)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3920), byte(1), line(s, 3920)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3921), byte(0), line(s, 3921)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [162, 0, 0, 100]
  s.ins MSGSET, uint(3922), byte(1), line(s, 3922)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3923), byte(1), line(s, 3923)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3924), byte(1), line(s, 3924)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3925), byte(1), line(s, 3925)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3926), byte(1), line(s, 3926)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3927), byte(1), line(s, 3927)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 25, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3928), byte(0), line(s, 3928)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3929), byte(0), line(s, 3929)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 32, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3930), byte(2), line(s, 3930)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 110, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3931), byte(1), line(s, 3931)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3932), byte(2), line(s, 3932)
  s.ins MSGSYNC, byte(0), 6229
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3933), byte(3), line(s, 3933)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3934), byte(2), line(s, 3934)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3935), byte(0), line(s, 3935)
  s.ins MSGSYNC, byte(0), 1888
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3936), byte(3), line(s, 3936)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3937), byte(2), line(s, 3937)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3938), byte(3), line(s, 3938)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3939), byte(3), line(s, 3939)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3940), byte(2), line(s, 3940)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3941), byte(3), line(s, 3941)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [170]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3942), byte(1), line(s, 3942)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3943), byte(1), line(s, 3943)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3944), byte(0), line(s, 3944)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3945), byte(0), line(s, 3945)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3946), byte(2), line(s, 3946)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3947), byte(1), line(s, 3947)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3948), byte(3), line(s, 3948)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3949), byte(1), line(s, 3949)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3950), byte(3), line(s, 3950)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3951), byte(3), line(s, 3951)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(3952), byte(0), line(s, 3952)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 19, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3953), byte(2), line(s, 3953)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [142]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3954), byte(1), line(s, 3954)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 70, 100, 2000, 0]
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x5914, [170, 0, 0, 100]
  s.ins MSGSET, uint(3955), byte(1), line(s, 3955)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3956), byte(1), line(s, 3956)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3957), byte(2), line(s, 3957)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins MSGSET, uint(3958), byte(127), line(s, 3958)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3959), byte(127), line(s, 3959)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [396, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 396
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3960), byte(0), line(s, 3960)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [151, 0, 0, 100]
  s.ins MSGSET, uint(3961), byte(1), line(s, 3961)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3962), byte(0), line(s, 3962)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3963), byte(1), line(s, 3963)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3964), byte(1), line(s, 3964)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3965), byte(1), line(s, 3965)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3966), byte(0), line(s, 3966)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3967), byte(1), line(s, 3967)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3968), byte(0), line(s, 3968)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3969), byte(2), line(s, 3969)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3970), byte(3), line(s, 3970)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3971), byte(0), line(s, 3971)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3972), byte(0), line(s, 3972)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3973), byte(0), line(s, 3973)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [142, 0, 0, 100]
  s.ins MSGSET, uint(3974), byte(1), line(s, 3974)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3975), byte(0), line(s, 3975)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [4, 73, 70, 1000, 0]
  s.ins MSGSET, uint(3976), byte(0), line(s, 3976)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3977), byte(2), line(s, 3977)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3978), byte(1), line(s, 3978)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3979), byte(3), line(s, 3979)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3980), byte(0), line(s, 3980)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3981), byte(1), line(s, 3981)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3982), byte(1), line(s, 3982)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins MSGSET, uint(3983), byte(127), line(s, 3983)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3984), byte(127), line(s, 3984)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(3985), byte(127), line(s, 3985)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [139]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [20, 20, 2200]
  s.ins CALL, :addr_0x542b, [20, 2200]
  s.ins MSGSET, uint(3986), byte(1), line(s, 3986)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [156]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [20, 20, 1]
  s.ins CALL, :addr_0x542b, [20, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [800, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-399, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 12291, 0]
  s.ins MSGSET, uint(3987), byte(1), line(s, 3987)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3988), byte(2), line(s, 3988)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3989), byte(127), line(s, 3989)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3990), byte(3), line(s, 3990)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3991), byte(3), line(s, 3991)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3992), byte(2), line(s, 3992)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3993), byte(3), line(s, 3993)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3994), byte(2), line(s, 3994)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x604e, [1300]
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins MSGSET, uint(3995), byte(127), line(s, 3995)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3996), byte(127), line(s, 3996)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3997), byte(127), line(s, 3997)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3998), byte(127), line(s, 3998)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(3999), byte(127), line(s, 3999)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 70, 1000]
  s.ins CALL, :f_se_play, [4, 73, 100, 0, 0]
  s.ins MSGSET, uint(4000), byte(127), line(s, 4000)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4001), byte(127), line(s, 4001)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4002), byte(127), line(s, 4002)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4003), byte(127), line(s, 4003)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4004), byte(127), line(s, 4004)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4005), byte(127), line(s, 4005)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [477]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [564, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 564
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(4006), byte(0), line(s, 4006)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4007), byte(1), line(s, 4007)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [4, 70, 100, 0, 0]
  s.ins MSGSET, uint(4008), byte(127), line(s, 4008)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4009), byte(127), line(s, 4009)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4010), byte(127), line(s, 4010)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4011), byte(127), line(s, 4011)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4012), byte(0), line(s, 4012)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [156]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [110, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins MSGSET, uint(4013), byte(1), line(s, 4013)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(0)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 212, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x6f17, [3, 60, 1000]
  s.ins MSGSET, uint(4014), byte(2), line(s, 4014)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4015), byte(3), line(s, 4015)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4016), byte(2), line(s, 4016)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 10, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4017), byte(3), line(s, 4017)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x604e, [1700]
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [499, Register.new(243), 1400]
  s.ins MOV, byte(0), ushort(243), 499
  s.ins LAYERCTRL, -6, 5, byte(1), 1700
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 500
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 36, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MSGSET, uint(4018), byte(127), line(s, 4018)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4019), byte(127), line(s, 4019)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(243)]
  s.ins MOV, byte(0), ushort(243), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [156]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins MSGSET, uint(4020), byte(1), line(s, 4020)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4021), byte(2), line(s, 4021)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4022), byte(127), line(s, 4022)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4023), byte(127), line(s, 4023)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [477]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4024), byte(127), line(s, 4024)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4025), byte(127), line(s, 4025)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4026), byte(127), line(s, 4026)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4027), byte(127), line(s, 4027)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [156]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [300, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(243)]
  s.ins MOV, byte(0), ushort(243), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4028), byte(1), line(s, 4028)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4029), byte(2), line(s, 4029)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4030), byte(3), line(s, 4030)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4031), byte(127), line(s, 4031)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4032), byte(127), line(s, 4032)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4033), byte(127), line(s, 4033)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4034), byte(0), line(s, 4034)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 3000]
  s.ins CALL, :addr_0x4eba, [3000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x77ee5
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x77ee5
  s.ins MOV, byte(0), ushort(119), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A07 start.', byte(0)
  s.label :game_05_gameB前半07
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7818a
  s.label :addr_0x78002
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7818a
  s.label :addr_0x780b5
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7818a
  s.label :addr_0x7818a
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4035), byte(2), line(s, 4035)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4036), byte(2), line(s, 4036)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [201, 0, 0, 100]
  s.ins MSGSET, uint(4037), byte(2), line(s, 4037)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [4, 73, 60, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 0, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 12, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 14, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4038), byte(1), line(s, 4038)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins MSGSET, uint(4039), byte(1), line(s, 4039)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4040), byte(0), line(s, 4040)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4041), byte(3), line(s, 4041)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 139, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4042), byte(2), line(s, 4042)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4043), byte(1), line(s, 4043)
  s.ins MSGSYNC, byte(0), 2410
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 3498
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [211]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins MSGSET, uint(4044), byte(2), line(s, 4044)
  s.ins MSGSYNC, byte(0), 2640
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4045), byte(2), line(s, 4045)
  s.ins MSGSYNC, byte(0), 965
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(4046), byte(2), line(s, 4046)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4047), byte(1), line(s, 4047)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4048), byte(2), line(s, 4048)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4049), byte(2), line(s, 4049)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4050), byte(0), line(s, 4050)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [224, 0, 0, 100]
  s.ins MSGSET, uint(4051), byte(2), line(s, 4051)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4052), byte(2), line(s, 4052)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4053), byte(0), line(s, 4053)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4054), byte(1), line(s, 4054)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4055), byte(0), line(s, 4055)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [224]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4056), byte(2), line(s, 4056)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4057), byte(2), line(s, 4057)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4058), byte(0), line(s, 4058)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4059), byte(2), line(s, 4059)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [395, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 395
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [600, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4060), byte(0), line(s, 4060)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4061), byte(0), line(s, 4061)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4062), byte(2), line(s, 4062)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [407, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 407
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [105, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(4063), byte(0), line(s, 4063)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(4064), byte(2), line(s, 4064)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4065), byte(1), line(s, 4065)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4066), byte(1), line(s, 4066)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 13, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4067), byte(0), line(s, 4067)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4068), byte(0), line(s, 4068)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4069), byte(3), line(s, 4069)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 73, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [306, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 306
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4070), byte(2), line(s, 4070)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4071), byte(0), line(s, 4071)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4072), byte(0), line(s, 4072)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4073), byte(2), line(s, 4073)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4074), byte(2), line(s, 4074)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4075), byte(3), line(s, 4075)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4076), byte(2), line(s, 4076)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [315, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 315
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4077), byte(2), line(s, 4077)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 108, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4078), byte(0), line(s, 4078)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4079), byte(0), line(s, 4079)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4080), byte(3), line(s, 4080)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [306, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 306
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4081), byte(2), line(s, 4081)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [315, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 315
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4082), byte(2), line(s, 4082)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [335, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 335
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4083), byte(2), line(s, 4083)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4084), byte(2), line(s, 4084)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [315, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 315
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4085), byte(2), line(s, 4085)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4086), byte(2), line(s, 4086)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [306, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 306
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4087), byte(2), line(s, 4087)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4088), byte(2), line(s, 4088)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4089), byte(2), line(s, 4089)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4090), byte(3), line(s, 4090)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4091), byte(3), line(s, 4091)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [335, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 335
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4092), byte(2), line(s, 4092)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4093), byte(2), line(s, 4093)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4094), byte(2), line(s, 4094)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [306, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 306
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4095), byte(2), line(s, 4095)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4096), byte(2), line(s, 4096)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4097), byte(3), line(s, 4097)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [335, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 335
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4098), byte(2), line(s, 4098)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins CALL, :addr_0x637e, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [224]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4099), byte(2), line(s, 4099)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x639f, []
  s.ins MSGSET, uint(4100), byte(2), line(s, 4100)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [208, 0, -299, 150]
  s.ins MSGSET, uint(4101), byte(2), line(s, 4101)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4102), byte(3), line(s, 4102)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [224, 0, -299, 150]
  s.ins MSGSET, uint(4103), byte(2), line(s, 4103)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4104), byte(2), line(s, 4104)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4105), byte(2), line(s, 4105)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4106), byte(127), line(s, 4106)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 3000]
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :f_se_play, [4, 59, 100, 2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4107), byte(2), line(s, 4107)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4108), byte(0), line(s, 4108)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4109), byte(0), line(s, 4109)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4110), byte(3), line(s, 4110)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4111), byte(0), line(s, 4111)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4112), byte(3), line(s, 4112)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4113), byte(3), line(s, 4113)
  s.ins MSGSYNC, byte(0), 7957
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4114), byte(0), line(s, 4114)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4115), byte(3), line(s, 4115)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4116), byte(3), line(s, 4116)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 130, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4117), byte(3), line(s, 4117)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4118), byte(3), line(s, 4118)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4119), byte(3), line(s, 4119)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4120), byte(0), line(s, 4120)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4121), byte(3), line(s, 4121)
  s.ins MSGSYNC, byte(0), 6869
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 29, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4122), byte(1), line(s, 4122)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [335, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 335
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4123), byte(2), line(s, 4123)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_foreground_picture, [362, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 362
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MSGSET, uint(4124), byte(1), line(s, 4124)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [300, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 300
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4125), byte(2), line(s, 4125)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4126), byte(2), line(s, 4126)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4127), byte(2), line(s, 4127)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [314, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 314
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-30, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [120, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4128), byte(2), line(s, 4128)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4129), byte(2), line(s, 4129)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 73, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 32, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4130), byte(1), line(s, 4130)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins LAYERSELECT, 87, 87
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [489, Register.new(246), 1400]
  s.ins MOV, byte(0), ushort(246), 489
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 300
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 0]
  s.ins MSGSET, uint(4131), byte(3), line(s, 4131)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 300
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 129, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [300, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 0]
  s.ins MSGSET, uint(4132), byte(0), line(s, 4132)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 44, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4133), byte(2), line(s, 4133)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 130, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4134), byte(1), line(s, 4134)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSET, uint(4135), byte(3), line(s, 4135)
  s.ins MSGSYNC, byte(0), 4906
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 500]
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 87, 87
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(246)]
  s.ins MOV, byte(0), ushort(246), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [478]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4136), byte(127), line(s, 4136)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4137), byte(127), line(s, 4137)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4138), byte(127), line(s, 4138)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4139), byte(127), line(s, 4139)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4140), byte(127), line(s, 4140)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4141), byte(127), line(s, 4141)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4142), byte(127), line(s, 4142)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4143), byte(127), line(s, 4143)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4144), byte(127), line(s, 4144)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4145), byte(127), line(s, 4145)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4146), byte(127), line(s, 4146)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [479]
  s.ins MSGSET, uint(4147), byte(2), line(s, 4147)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x7b5d4
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x7b5d4
  s.ins MOV, byte(0), ushort(120), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A08 start.', byte(0)
  s.label :game_05_gameB前半08
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(181)]
  s.ins MOV, byte(0), ushort(181), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [229]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7b9c2
  s.label :addr_0x7b76e
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [229]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7b9c2
  s.label :addr_0x7b898
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [229]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7b9c2
  s.label :addr_0x7b9c2
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4148), byte(2), line(s, 4148)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4149), byte(3), line(s, 4149)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4150), byte(0), line(s, 4150)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4151), byte(127), line(s, 4151)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGSET, uint(4152), byte(127), line(s, 4152)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 21, 100, 0, 0]
  s.ins CALL, :addr_0x5cac, [0, 153, 0, 0, 100]
  s.ins MSGSET, uint(4153), byte(127), line(s, 4153)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4154), byte(1), line(s, 4154)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [174, 0, 0, 100]
  s.ins MSGSET, uint(4155), byte(1), line(s, 4155)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 43, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4156), byte(0), line(s, 4156)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4157), byte(0), line(s, 4157)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4158), byte(0), line(s, 4158)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4159), byte(0), line(s, 4159)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4160), byte(0), line(s, 4160)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 9, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4161), byte(2), line(s, 4161)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4162), byte(2), line(s, 4162)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4163), byte(0), line(s, 4163)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4164), byte(2), line(s, 4164)
  s.ins MSGSYNC, byte(0), 234
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 1000]
  s.ins MSGSYNC, byte(0), 1248
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSYNC, byte(0), 2976
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 41, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4165), byte(2), line(s, 4165)
  s.ins MSGSYNC, byte(0), 192
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSYNC, byte(0), 2112
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSYNC, byte(0), 6016
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 136, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4166), byte(0), line(s, 4166)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4167), byte(0), line(s, 4167)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 121, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4168), byte(3), line(s, 4168)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4169), byte(0), line(s, 4169)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4170), byte(0), line(s, 4170)
  s.ins MSGSYNC, byte(0), 4032
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4171), byte(3), line(s, 4171)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [174]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4172), byte(1), line(s, 4172)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [376, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 376
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4173), byte(0), line(s, 4173)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [149]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4174), byte(1), line(s, 4174)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [384, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 384
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4175), byte(0), line(s, 4175)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4176), byte(3), line(s, 4176)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 0, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4177), byte(0), line(s, 4177)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4178), byte(3), line(s, 4178)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4179), byte(0), line(s, 4179)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 132, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4180), byte(1), line(s, 4180)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4181), byte(127), line(s, 4181)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 50, 1000]
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 119, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-99, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4182), byte(0), line(s, 4182)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4183), byte(3), line(s, 4183)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4184), byte(0), line(s, 4184)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4185), byte(3), line(s, 4185)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4186), byte(0), line(s, 4186)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4187), byte(0), line(s, 4187)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4188), byte(0), line(s, 4188)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4189), byte(3), line(s, 4189)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4190), byte(0), line(s, 4190)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 26, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4191), byte(0), line(s, 4191)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4192), byte(0), line(s, 4192)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4193), byte(0), line(s, 4193)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4194), byte(0), line(s, 4194)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4195), byte(3), line(s, 4195)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4196), byte(0), line(s, 4196)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4197), byte(0), line(s, 4197)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4198), byte(0), line(s, 4198)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4199), byte(127), line(s, 4199)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4200), byte(127), line(s, 4200)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4201), byte(3), line(s, 4201)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 37, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4202), byte(1), line(s, 4202)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4203), byte(3), line(s, 4203)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4204), byte(3), line(s, 4204)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4205), byte(3), line(s, 4205)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [155]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(4206), byte(1), line(s, 4206)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4207), byte(1), line(s, 4207)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4208), byte(1), line(s, 4208)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 41, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-149, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4209), byte(2), line(s, 4209)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 100
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 119, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [150, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 1]
  s.ins MSGSET, uint(4210), byte(0), line(s, 4210)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4211), byte(0), line(s, 4211)
  s.ins MSGSYNC, byte(0), 746
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4212), byte(0), line(s, 4212)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 43, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4213), byte(0), line(s, 4213)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4214), byte(0), line(s, 4214)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [155]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4215), byte(1), line(s, 4215)
  s.ins MSGSYNC, byte(0), 5237
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-149, 0, 0, 0]
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [397, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 397
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4216), byte(0), line(s, 4216)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4217), byte(0), line(s, 4217)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4218), byte(0), line(s, 4218)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_set_sprite_x_offset, [-399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(4219), byte(1), line(s, 4219)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4220), byte(127), line(s, 4220)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 16, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 41, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4221), byte(0), line(s, 4221)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4222), byte(2), line(s, 4222)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4223), byte(0), line(s, 4223)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4224), byte(3), line(s, 4224)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4225), byte(0), line(s, 4225)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4226), byte(0), line(s, 4226)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4227), byte(127), line(s, 4227)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4228), byte(127), line(s, 4228)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4229), byte(0), line(s, 4229)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4230), byte(3), line(s, 4230)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [155]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4231), byte(1), line(s, 4231)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4232), byte(1), line(s, 4232)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 69, 100, 5000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins PLANESELECT, byte(2)
  s.ins MASKLOAD, 77, 0, 0
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 508
  s.ins LAYERCTRL, -4, 18, byte(1), 42
  s.ins PLANESELECT, byte(1)
  s.ins MASKLOAD, 79, 0, 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [155]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins PLANESELECT, byte(0)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [10]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 35, 0, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4233), byte(3), line(s, 4233)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4234), byte(3), line(s, 4234)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4235), byte(1), line(s, 4235)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [155]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :f_hide_all_sprites, []
  s.ins PLANESELECT, byte(2)
  s.ins PLANECLEAR
  s.ins LAYERINIT, -4
  s.ins PLANESELECT, byte(1)
  s.ins PLANECLEAR
  s.ins PLANESELECT, byte(0)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [399, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 399
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4236), byte(0), line(s, 4236)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [387, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 387
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-99, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4237), byte(0), line(s, 4237)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4238), byte(1), line(s, 4238)
  s.ins MSGSYNC, byte(0), 1440
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_set_sprite_x_offset, [-399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x7ea71
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x7ea71
  s.ins MOV, byte(0), ushort(121), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A09 start.', byte(0)
  s.label :game_05_gameB前半09
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [220]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7ed10
  s.label :addr_0x7eb8c
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [220]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7ed10
  s.label :addr_0x7ec3d
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [220]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x7ed10
  s.label :addr_0x7ed10
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4239), byte(2), line(s, 4239)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4240), byte(2), line(s, 4240)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4241), byte(127), line(s, 4241)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4242), byte(127), line(s, 4242)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [370, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 370
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4243), byte(1), line(s, 4243)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4244), byte(1), line(s, 4244)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4245), byte(1), line(s, 4245)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 10, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 111, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4246), byte(0), line(s, 4246)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 112, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4247), byte(2), line(s, 4247)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4248), byte(3), line(s, 4248)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4249), byte(2), line(s, 4249)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 130, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4250), byte(3), line(s, 4250)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4251), byte(2), line(s, 4251)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins CALL, :addr_0x637e, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [223]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins MSGSET, uint(4252), byte(2), line(s, 4252)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x639f, []
  s.ins MSGSET, uint(4253), byte(2), line(s, 4253)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4254), byte(2), line(s, 4254)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4255), byte(0), line(s, 4255)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [306, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 306
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4256), byte(2), line(s, 4256)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4257), byte(2), line(s, 4257)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4258), byte(2), line(s, 4258)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4259), byte(2), line(s, 4259)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [315, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 315
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4260), byte(2), line(s, 4260)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4261), byte(2), line(s, 4261)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4262), byte(2), line(s, 4262)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4263), byte(2), line(s, 4263)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [223]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4264), byte(2), line(s, 4264)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4265), byte(2), line(s, 4265)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4266), byte(2), line(s, 4266)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4267), byte(2), line(s, 4267)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4268), byte(127), line(s, 4268)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4269), byte(127), line(s, 4269)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [346, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 346
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4270), byte(1), line(s, 4270)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4271), byte(1), line(s, 4271)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4272), byte(127), line(s, 4272)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [528, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 528
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4273), byte(127), line(s, 4273)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4274), byte(3), line(s, 4274)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4275), byte(1), line(s, 4275)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4276), byte(0), line(s, 4276)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4277), byte(1), line(s, 4277)
  s.ins MSGSYNC, byte(0), 4032
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4278), byte(1), line(s, 4278)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4279), byte(3), line(s, 4279)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4280), byte(3), line(s, 4280)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4281), byte(3), line(s, 4281)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4282), byte(0), line(s, 4282)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4283), byte(3), line(s, 4283)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [224]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4284), byte(2), line(s, 4284)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4285), byte(2), line(s, 4285)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4286), byte(2), line(s, 4286)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [211, 0, -349, 150]
  s.ins MSGSET, uint(4287), byte(2), line(s, 4287)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4288), byte(3), line(s, 4288)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4289), byte(0), line(s, 4289)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4290), byte(0), line(s, 4290)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4291), byte(127), line(s, 4291)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-15, 500, 131, 0]
  s.ins MSGSET, uint(4292), byte(127), line(s, 4292)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-15, 200, 133, -1]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [202]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4293), byte(2), line(s, 4293)
  s.ins MSGSYNC, byte(0), 3776
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [481]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :f_se_play, [0, 67, 100, 0, 0]
  s.ins MSGSET, uint(4294), byte(127), line(s, 4294)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4295), byte(127), line(s, 4295)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 79, 100, 0, 0]
  s.ins MSGSET, uint(4296), byte(127), line(s, 4296)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [16, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-7999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 17, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 121, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 109, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [400, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4297), byte(3), line(s, 4297)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4298), byte(0), line(s, 4298)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4299), byte(4), line(s, 4299)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4300), byte(127), line(s, 4300)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4301), byte(1), line(s, 4301)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [225]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4302), byte(2), line(s, 4302)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4303), byte(2), line(s, 4303)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4304), byte(1), line(s, 4304)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4305), byte(2), line(s, 4305)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4306), byte(1), line(s, 4306)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4307), byte(2), line(s, 4307)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4308), byte(2), line(s, 4308)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [215]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [105, 0, 0, 0]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [100]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 400]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [100, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-699, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 400, 12291, 0]
  s.ins MSGSET, uint(4309), byte(2), line(s, 4309)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4310), byte(127), line(s, 4310)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4311), byte(127), line(s, 4311)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4312), byte(127), line(s, 4312)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4313), byte(2), line(s, 4313)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x80f33
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x80f33
  s.ins MOV, byte(0), ushort(122), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A10 start.', byte(0)
  s.label :game_05_gameB前半10
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x8131c
  s.label :addr_0x81067
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x8131c
  s.label :addr_0x811e6
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x8131c
  s.label :addr_0x8131c
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4314), byte(1), line(s, 4314)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4315), byte(0), line(s, 4315)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 110, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4316), byte(2), line(s, 4316)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4317), byte(1), line(s, 4317)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 134, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4318), byte(3), line(s, 4318)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 45, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4319), byte(2), line(s, 4319)
  s.ins MSGSYNC, byte(0), 2130
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 39, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 18, 100, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [602, Register.new(191)]
  s.ins MOV, byte(0), ushort(191), 602
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(4320), byte(127), line(s, 4320)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4321), byte(127), line(s, 4321)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4322), byte(0), line(s, 4322)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4323), byte(2), line(s, 4323)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4324), byte(2), line(s, 4324)
  s.ins MSGSYNC, byte(0), 4832
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4325), byte(2), line(s, 4325)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4326), byte(1), line(s, 4326)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 27, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4327), byte(2), line(s, 4327)
  s.ins MSGSYNC, byte(0), 2517
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 43, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4328), byte(2), line(s, 4328)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [257]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4329), byte(3), line(s, 4329)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [304, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 304
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4330), byte(2), line(s, 4330)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4331), byte(2), line(s, 4331)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4332), byte(2), line(s, 4332)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4333), byte(2), line(s, 4333)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [482]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSET, uint(4334), byte(2), line(s, 4334)
  s.ins MSGSYNC, byte(0), 2336
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 4901
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSET, uint(4335), byte(2), line(s, 4335)
  s.ins MSGSYNC, byte(0), 2800
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 5189
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSET, uint(4336), byte(1), line(s, 4336)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4337), byte(2), line(s, 4337)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4338), byte(0), line(s, 4338)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4339), byte(2), line(s, 4339)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4340), byte(2), line(s, 4340)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4341), byte(2), line(s, 4341)
  s.ins MSGSYNC, byte(0), 8170
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4342), byte(1), line(s, 4342)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4343), byte(127), line(s, 4343)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4344), byte(127), line(s, 4344)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSET, uint(4345), byte(2), line(s, 4345)
  s.ins MSGSYNC, byte(0), 2229
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 4021
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 4928
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 6005
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4346), byte(3), line(s, 4346)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4347), byte(2), line(s, 4347)
  s.ins MSGSYNC, byte(0), 1546
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGSYNC, byte(0), 2186
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 450]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 40, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 7, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4348), byte(1), line(s, 4348)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4349), byte(0), line(s, 4349)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4350), byte(127), line(s, 4350)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4351), byte(127), line(s, 4351)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4352), byte(127), line(s, 4352)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [344, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 344
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [266]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4353), byte(2), line(s, 4353)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [244, -479, 0, 100]
  s.ins MSGSET, uint(4354), byte(3), line(s, 4354)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4355), byte(3), line(s, 4355)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [310, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 310
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [392, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [310, Register.new(212)]
  s.ins MOV, byte(0), ushort(212), 310
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [392, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins MSGSET, uint(4356), byte(2), line(s, 4356)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [482]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 2, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [200]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [257]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4357), byte(3), line(s, 4357)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4358), byte(3), line(s, 4358)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4359), byte(2), line(s, 4359)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [244, 0, 0, 100]
  s.ins MSGSET, uint(4360), byte(3), line(s, 4360)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4361), byte(2), line(s, 4361)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 28, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4362), byte(1), line(s, 4362)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4363), byte(127), line(s, 4363)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4364), byte(127), line(s, 4364)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [603, Register.new(191)]
  s.ins MOV, byte(0), ushort(191), 603
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [50, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-10, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [603, Register.new(192)]
  s.ins MOV, byte(0), ushort(192), 603
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-99, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [30, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [45, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-8, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [0, 10, 100, 0, 0]
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [3500, 200, 133, 1]
  s.ins CALL, :f_set_sprite_y_offset, [-549, 200, 5, 1]
  s.ins CALL, :f_set_sprite_zoom, [200, 200, 5, 1]
  s.ins CALL, :addr_0x51ce, [20]
  s.ins CALL, :addr_0x51dc, [5]
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [3500, 200, 133, 1]
  s.ins CALL, :f_set_sprite_y_offset, [-749, 200, 5, 1]
  s.ins CALL, :f_set_sprite_zoom, [180, 200, 5, 1]
  s.ins CALL, :addr_0x51ce, [20]
  s.ins CALL, :addr_0x51dc, [5]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(192)]
  s.ins MOV, byte(0), ushort(192), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [483]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [0, 13, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(4365), byte(2), line(s, 4365)
  s.ins MSGSYNC, byte(0), 2896
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 13, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 4581
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins CALL, :f_se_play, [0, 13, 100, 0, 0]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 12, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [250]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [700]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [601, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 601
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [950]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [850]
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins CALL, :addr_0x5184, [255, 0, 0, 0]
  s.ins CALL, :addr_0x6e1d, [0, 200]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4366), byte(2), line(s, 4366)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4367), byte(2), line(s, 4367)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4368), byte(2), line(s, 4368)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [266]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [400, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4369), byte(3), line(s, 4369)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4370), byte(127), line(s, 4370)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4371), byte(127), line(s, 4371)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4372), byte(127), line(s, 4372)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4373), byte(3), line(s, 4373)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [344, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 344
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4374), byte(2), line(s, 4374)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4375), byte(2), line(s, 4375)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4376), byte(3), line(s, 4376)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4377), byte(3), line(s, 4377)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [327, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 327
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MSGSET, uint(4378), byte(2), line(s, 4378)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins MSGSET, uint(4379), byte(127), line(s, 4379)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4380), byte(127), line(s, 4380)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 130, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [2, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4381), byte(2), line(s, 4381)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4382), byte(2), line(s, 4382)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4383), byte(1), line(s, 4383)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 223, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4384), byte(2), line(s, 4384)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 225, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4385), byte(2), line(s, 4385)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4386), byte(1), line(s, 4386)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 130, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4387), byte(1), line(s, 4387)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSET, uint(4388), byte(2), line(s, 4388)
  s.ins MSGSYNC, byte(0), 2602
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 241, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 235, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4389), byte(2), line(s, 4389)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 241, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4390), byte(2), line(s, 4390)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4391), byte(1), line(s, 4391)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4392), byte(2), line(s, 4392)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGSET, uint(4393), byte(1), line(s, 4393)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4394), byte(2), line(s, 4394)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4395), byte(1), line(s, 4395)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4396), byte(1), line(s, 4396)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4397), byte(0), line(s, 4397)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4398), byte(1), line(s, 4398)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4399), byte(1), line(s, 4399)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4400), byte(2), line(s, 4400)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4401), byte(1), line(s, 4401)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 40, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4402), byte(2), line(s, 4402)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4403), byte(1), line(s, 4403)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4404), byte(1), line(s, 4404)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4405), byte(1), line(s, 4405)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4406), byte(1), line(s, 4406)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4407), byte(2), line(s, 4407)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4408), byte(0), line(s, 4408)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 34, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4409), byte(0), line(s, 4409)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4410), byte(1), line(s, 4410)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4411), byte(1), line(s, 4411)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 135, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4412), byte(2), line(s, 4412)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4413), byte(2), line(s, 4413)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4414), byte(1), line(s, 4414)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4415), byte(1), line(s, 4415)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4416), byte(2), line(s, 4416)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 39, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4417), byte(127), line(s, 4417)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4418), byte(127), line(s, 4418)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4419), byte(127), line(s, 4419)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4420), byte(1), line(s, 4420)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 137, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4421), byte(2), line(s, 4421)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4422), byte(1), line(s, 4422)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4423), byte(1), line(s, 4423)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4424), byte(1), line(s, 4424)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 33, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4425), byte(2), line(s, 4425)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [362, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 362
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [362, Register.new(212)]
  s.ins MOV, byte(0), ushort(212), 362
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(4426), byte(1), line(s, 4426)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4427), byte(2), line(s, 4427)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4428), byte(1), line(s, 4428)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4429), byte(1), line(s, 4429)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4430), byte(2), line(s, 4430)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 140, 1, 0, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 12288, 0]
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4431), byte(0), line(s, 4431)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 32, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4432), byte(1), line(s, 4432)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4433), byte(1), line(s, 4433)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 41, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4434), byte(2), line(s, 4434)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4435), byte(1), line(s, 4435)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4436), byte(1), line(s, 4436)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4437), byte(2), line(s, 4437)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4438), byte(1), line(s, 4438)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4439), byte(2), line(s, 4439)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4440), byte(1), line(s, 4440)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4441), byte(2), line(s, 4441)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4442), byte(1), line(s, 4442)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [356, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 356
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-459, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4443), byte(1), line(s, 4443)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_foreground_picture, [317, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 317
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [430, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins MSGSET, uint(4444), byte(2), line(s, 4444)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4445), byte(0), line(s, 4445)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(4446), byte(0), line(s, 4446)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x85bb9
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x85bb9
  s.ins MOV, byte(0), ushort(123), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A11 start.', byte(0)
  s.label :game_05_gameB前半11
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x85f87
  s.label :addr_0x85cef
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x85f87
  s.label :addr_0x85e27
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x85f87
  s.label :addr_0x85f87
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4447), byte(127), line(s, 4447)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [528, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 528
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4448), byte(127), line(s, 4448)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 16, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4449), byte(0), line(s, 4449)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4450), byte(0), line(s, 4450)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 121, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4451), byte(2), line(s, 4451)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4452), byte(2), line(s, 4452)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4453), byte(2), line(s, 4453)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [241]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4454), byte(3), line(s, 4454)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [244, 0, 0, 100]
  s.ins MSGSET, uint(4455), byte(3), line(s, 4455)
  s.ins MSGSYNC, byte(0), 4597
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [326, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 326
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4456), byte(2), line(s, 4456)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [330, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 330
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4457), byte(2), line(s, 4457)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [324, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 324
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4458), byte(2), line(s, 4458)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4459), byte(1), line(s, 4459)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERCTRL, -6, 5, byte(1), 1900
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1700
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [581, Register.new(237), 1400]
  s.ins MOV, byte(0), ushort(237), 581
  s.ins CALL, :f_set_sprite_x_offset, [-319, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins CALL, :f_set_sprite_x_offset, [320, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4460), byte(127), line(s, 4460)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins CALL, :addr_0x51ea, [192, 0, 0, 0]
  s.ins MSGSET, uint(4461), byte(127), line(s, 4461)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(237)]
  s.ins MOV, byte(0), ushort(237), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(238)]
  s.ins MOV, byte(0), ushort(238), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 141, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4462), byte(0), line(s, 4462)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4463), byte(0), line(s, 4463)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 3, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4464), byte(2), line(s, 4464)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4465), byte(0), line(s, 4465)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4466), byte(0), line(s, 4466)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 37, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4467), byte(1), line(s, 4467)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4468), byte(0), line(s, 4468)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4469), byte(0), line(s, 4469)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 42, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4470), byte(2), line(s, 4470)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4471), byte(0), line(s, 4471)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4472), byte(0), line(s, 4472)
  s.ins MSGSYNC, byte(0), 4746
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4473), byte(3), line(s, 4473)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4474), byte(0), line(s, 4474)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4475), byte(1), line(s, 4475)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [311, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 311
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [311, Register.new(212)]
  s.ins MOV, byte(0), ushort(212), 311
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins MSGSET, uint(4476), byte(2), line(s, 4476)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4477), byte(2), line(s, 4477)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4478), byte(2), line(s, 4478)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [394, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 394
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-479, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4479), byte(0), line(s, 4479)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [344, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 344
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4480), byte(2), line(s, 4480)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4481), byte(2), line(s, 4481)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [317, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 317
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [430, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1700
  s.ins MSGSET, uint(4482), byte(2), line(s, 4482)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [393, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 393
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-419, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins MSGSET, uint(4483), byte(0), line(s, 4483)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 500
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :addr_0x4ff1, [253]
  s.ins MSGSET, uint(4484), byte(3), line(s, 4484)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4485), byte(127), line(s, 4485)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4486), byte(127), line(s, 4486)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4487), byte(127), line(s, 4487)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(4488), byte(3), line(s, 4488)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4489), byte(0), line(s, 4489)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGSET, uint(4490), byte(127), line(s, 4490)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 21, 100, 0, 0]
  s.ins CALL, :addr_0x5cac, [0, 177, 0, 0, 100]
  s.ins MSGSET, uint(4491), byte(127), line(s, 4491)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4492), byte(2), line(s, 4492)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4493), byte(1), line(s, 4493)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4494), byte(3), line(s, 4494)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4495), byte(0), line(s, 4495)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins MSGSET, uint(4496), byte(2), line(s, 4496)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4497), byte(3), line(s, 4497)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4498), byte(3), line(s, 4498)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 800
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 34, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4499), byte(127), line(s, 4499)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4500), byte(3), line(s, 4500)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 1, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4501), byte(0), line(s, 4501)
  s.ins MSGSYNC, byte(0), 1248
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4502), byte(3), line(s, 4502)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4503), byte(3), line(s, 4503)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 37, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4504), byte(1), line(s, 4504)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4505), byte(3), line(s, 4505)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4506), byte(3), line(s, 4506)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 234, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4507), byte(2), line(s, 4507)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4508), byte(2), line(s, 4508)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 8, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4509), byte(3), line(s, 4509)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4510), byte(3), line(s, 4510)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 14, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4511), byte(0), line(s, 4511)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4512), byte(3), line(s, 4512)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 35, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4513), byte(2), line(s, 4513)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4514), byte(3), line(s, 4514)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4515), byte(3), line(s, 4515)
  s.ins MSGSYNC, byte(0), 5418
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4516), byte(3), line(s, 4516)
  s.ins MSGSYNC, byte(0), 5717
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [165]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4517), byte(1), line(s, 4517)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4518), byte(1), line(s, 4518)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4519), byte(0), line(s, 4519)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x604e, [1900]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1900
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4520), byte(127), line(s, 4520)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4521), byte(127), line(s, 4521)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4522), byte(127), line(s, 4522)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4523), byte(127), line(s, 4523)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4524), byte(127), line(s, 4524)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4525), byte(0), line(s, 4525)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4526), byte(0), line(s, 4526)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1900
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_play, [0, 19, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [447]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [550, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-449, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERCTRL, -6, 22, byte(0)
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [603, Register.new(191)]
  s.ins MOV, byte(0), ushort(191), 603
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [50, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-10, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [603, Register.new(192)]
  s.ins MOV, byte(0), ushort(192), 603
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-99, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [30, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [45, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [8, 0, 0, 0]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [3500, 400, 133, 1]
  s.ins CALL, :f_set_sprite_y_offset, [-499, 400, 5, 1]
  s.ins CALL, :f_set_sprite_zoom, [180, 400, 5, 1]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins CALL, :addr_0x51dc, [5]
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-3499, 400, 133, 1]
  s.ins CALL, :f_set_sprite_y_offset, [-599, 400, 5, 1]
  s.ins CALL, :f_set_sprite_zoom, [180, 400, 5, 1]
  s.ins CALL, :addr_0x51ce, [10]
  s.ins CALL, :addr_0x51dc, [5]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 32, 32
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(191)]
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 33, 33
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(192)]
  s.ins MOV, byte(0), ushort(192), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins CALL, :addr_0x53e4, [50, 100, 400]
  s.ins MSGSET, uint(4527), byte(127), line(s, 4527)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [448, 550, -449, 200]
  s.ins MSGSET, uint(4528), byte(0), line(s, 4528)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4529), byte(3), line(s, 4529)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-99, 1000, 12291, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 1000, 12291, 0]
  s.ins MSGSET, uint(4530), byte(3), line(s, 4530)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 44, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [448]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-99, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MSGSET, uint(4531), byte(127), line(s, 4531)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [449]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [110, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(4532), byte(0), line(s, 4532)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4533), byte(1), line(s, 4533)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [450]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [110, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(4534), byte(2), line(s, 4534)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 250]
  s.ins MSGSET, uint(4535), byte(127), line(s, 4535)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 83, 100, 0, 0]
  s.ins CALL, :f_se_play, [1, 16, 100, 0, 0]
  s.ins MSGSET, uint(4536), byte(127), line(s, 4536)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4537), byte(127), line(s, 4537)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [1, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4538), byte(3), line(s, 4538)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4539), byte(3), line(s, 4539)
  s.ins MSGSYNC, byte(0), 5322
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [140]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4540), byte(1), line(s, 4540)
  s.ins MSGSYNC, byte(0), 4785
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSYNC, byte(0), 5758
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4541), byte(2), line(s, 4541)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4542), byte(3), line(s, 4542)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4543), byte(3), line(s, 4543)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4544), byte(3), line(s, 4544)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4545), byte(3), line(s, 4545)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4546), byte(3), line(s, 4546)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 35, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4547), byte(2), line(s, 4547)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 138, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4548), byte(3), line(s, 4548)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4549), byte(3), line(s, 4549)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 34, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4550), byte(3), line(s, 4550)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 131, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 137, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4551), byte(1), line(s, 4551)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4552), byte(2), line(s, 4552)
  s.ins MSGSYNC, byte(0), 5877
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [413, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 413
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4553), byte(3), line(s, 4553)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4554), byte(127), line(s, 4554)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_foreground_picture, [416, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 416
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4555), byte(127), line(s, 4555)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 16, 100, 0, 0]
  s.ins CALL, :f_se_play, [1, 20, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(184)]
  s.ins MOV, byte(0), ushort(184), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [474]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(4556), byte(127), line(s, 4556)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(4557), byte(127), line(s, 4557)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [12, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4558), byte(127), line(s, 4558)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4559), byte(127), line(s, 4559)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(13), 47
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [13, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 200, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 200, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 200, 0, 0]
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(4560), byte(127), line(s, 4560)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x89ac9
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x89ac9
  s.ins MOV, byte(0), ushort(124), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's05_gameB_A12 start.', byte(0)
  s.label :game_05_gameB前半12
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4561), byte(127), line(s, 4561)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x89e7f
  s.label :addr_0x89c3b
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins MSGSET, uint(4562), byte(127), line(s, 4562)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x89e7f
  s.label :addr_0x89d38
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins MSGSET, uint(4563), byte(127), line(s, 4563)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 53, 53
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(212)]
  s.ins MOV, byte(0), ushort(212), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [10, :null]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins JUMP, :addr_0x89e7f
  s.label :addr_0x89e7f
  s.ins MSGSET, uint(4564), byte(2), line(s, 4564)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4565), byte(127), line(s, 4565)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 9, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 130, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4566), byte(3), line(s, 4566)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4567), byte(3), line(s, 4567)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4568), byte(1), line(s, 4568)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4569), byte(3), line(s, 4569)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4570), byte(1), line(s, 4570)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4571), byte(3), line(s, 4571)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4572), byte(1), line(s, 4572)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4573), byte(1), line(s, 4573)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4574), byte(3), line(s, 4574)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4575), byte(3), line(s, 4575)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 13, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4576), byte(2), line(s, 4576)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4577), byte(3), line(s, 4577)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4578), byte(1), line(s, 4578)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4579), byte(1), line(s, 4579)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4580), byte(1), line(s, 4580)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4581), byte(1), line(s, 4581)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(4582), byte(3), line(s, 4582)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4583), byte(3), line(s, 4583)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4584), byte(3), line(s, 4584)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4585), byte(1), line(s, 4585)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4586), byte(1), line(s, 4586)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [211]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4587), byte(2), line(s, 4587)
  s.ins MSGSYNC, byte(0), 706
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4588), byte(2), line(s, 4588)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(4589), byte(2), line(s, 4589)
  s.ins MSGSYNC, byte(0), 2645
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2986
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 3338
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 1000]
  s.ins MSGSYNC, byte(0), 5984
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(4590), byte(2), line(s, 4590)
  s.ins MSGSYNC, byte(0), 1066
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2373
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 1000]
  s.ins CALL, :addr_0x542b, [30, 1000]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4591), byte(3), line(s, 4591)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [Register.new(26), 0, -199, 130]
  s.ins MSGSET, uint(4592), byte(2), line(s, 4592)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4593), byte(127), line(s, 4593)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [348, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 348
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4594), byte(1), line(s, 4594)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4595), byte(1), line(s, 4595)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4596), byte(1), line(s, 4596)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4597), byte(0), line(s, 4597)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(4598), byte(2), line(s, 4598)
  s.ins MSGSYNC, byte(0), 4209
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 1800]
  s.ins CALL, :addr_0x542b, [30, 1800]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4599), byte(3), line(s, 4599)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4600), byte(3), line(s, 4600)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 40, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4601), byte(2), line(s, 4601)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4602), byte(3), line(s, 4602)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4603), byte(3), line(s, 4603)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4604), byte(3), line(s, 4604)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 16, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4605), byte(1), line(s, 4605)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4606), byte(3), line(s, 4606)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4607), byte(3), line(s, 4607)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4608), byte(3), line(s, 4608)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4609), byte(3), line(s, 4609)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 3, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4610), byte(2), line(s, 4610)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4611), byte(3), line(s, 4611)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4612), byte(3), line(s, 4612)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4613), byte(3), line(s, 4613)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4614), byte(3), line(s, 4614)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4615), byte(3), line(s, 4615)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 13, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4616), byte(2), line(s, 4616)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4617), byte(3), line(s, 4617)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4618), byte(3), line(s, 4618)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4619), byte(3), line(s, 4619)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4620), byte(3), line(s, 4620)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4621), byte(1), line(s, 4621)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4622), byte(3), line(s, 4622)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4623), byte(3), line(s, 4623)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 1, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4624), byte(1), line(s, 4624)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 18, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4625), byte(1), line(s, 4625)
  s.ins MSGSYNC, byte(0), 648
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4626), byte(1), line(s, 4626)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4627), byte(1), line(s, 4627)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4628), byte(0), line(s, 4628)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4629), byte(1), line(s, 4629)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4630), byte(1), line(s, 4630)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4631), byte(0), line(s, 4631)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4632), byte(1), line(s, 4632)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4633), byte(2), line(s, 4633)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 132, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4634), byte(3), line(s, 4634)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 128, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [300, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4635), byte(127), line(s, 4635)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4636), byte(3), line(s, 4636)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4637), byte(0), line(s, 4637)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 31, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4638), byte(1), line(s, 4638)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4639), byte(3), line(s, 4639)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4640), byte(1), line(s, 4640)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [11, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 116, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4641), byte(0), line(s, 4641)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4642), byte(1), line(s, 4642)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4643), byte(3), line(s, 4643)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4644), byte(1), line(s, 4644)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4645), byte(3), line(s, 4645)
  s.ins MSGSYNC, byte(0), 1573
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSYNC, byte(0), 2741
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 130, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4646), byte(3), line(s, 4646)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4647), byte(3), line(s, 4647)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4648), byte(1), line(s, 4648)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4649), byte(0), line(s, 4649)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4650), byte(1), line(s, 4650)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4651), byte(1), line(s, 4651)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4652), byte(1), line(s, 4652)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4653), byte(1), line(s, 4653)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 113, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4654), byte(2), line(s, 4654)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4655), byte(0), line(s, 4655)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4656), byte(1), line(s, 4656)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4657), byte(3), line(s, 4657)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 14, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(4658), byte(0), line(s, 4658)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4659), byte(2), line(s, 4659)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4660), byte(127), line(s, 4660)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4661), byte(127), line(s, 4661)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4662), byte(127), line(s, 4662)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4663), byte(1), line(s, 4663)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 22, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [1]
  s.ins CALL, :addr_0x5cac, [1, 267, 0, 0, 100]
  s.ins MSGSET, uint(4664), byte(127), line(s, 4664)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [271, 0, 0, 100]
  s.ins MSGSET, uint(4665), byte(127), line(s, 4665)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4666), byte(1), line(s, 4666)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins MSGSET, uint(4667), byte(3), line(s, 4667)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4668), byte(1), line(s, 4668)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4669), byte(2), line(s, 4669)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 36, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4670), byte(1), line(s, 4670)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4671), byte(1), line(s, 4671)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 42, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4672), byte(1), line(s, 4672)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 35, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4673), byte(2), line(s, 4673)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4674), byte(1), line(s, 4674)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4675), byte(1), line(s, 4675)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [245]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4676), byte(3), line(s, 4676)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4677), byte(0), line(s, 4677)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4678), byte(0), line(s, 4678)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4679), byte(2), line(s, 4679)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [271, 0, 0, 100]
  s.ins MSGSET, uint(4680), byte(3), line(s, 4680)
  s.ins MSGSYNC, byte(0), 3626
  s.ins CALL, :addr_0x5914, [261, 0, 0, 100]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4681), byte(1), line(s, 4681)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [473]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4682), byte(127), line(s, 4682)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4683), byte(127), line(s, 4683)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4684), byte(127), line(s, 4684)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins MSGSET, uint(4685), byte(0), line(s, 4685)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4686), byte(1), line(s, 4686)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [16, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4687), byte(127), line(s, 4687)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4688), byte(127), line(s, 4688)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [249]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4689), byte(127), line(s, 4689)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4690), byte(127), line(s, 4690)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-1999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 115, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4691), byte(1), line(s, 4691)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 1
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4692), byte(127), line(s, 4692)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 101, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 24, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4693), byte(0), line(s, 4693)
  s.ins MSGSYNC, byte(0), 1442
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 1
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4694), byte(2), line(s, 4694)
  s.ins MSGSYNC, byte(0), 202
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSYNC, byte(0), 1157
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSYNC, byte(0), 2688
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 3381
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [571, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 571
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4695), byte(1), line(s, 4695)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_play, [0, 22, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :addr_0x5cac, [0, 233, 0, -299, 150]
  s.ins MSGSET, uint(4696), byte(127), line(s, 4696)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4697), byte(127), line(s, 4697)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 35, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4698), byte(1), line(s, 4698)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4699), byte(0), line(s, 4699)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [233]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4700), byte(2), line(s, 4700)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [212]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 5000]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 5000]
  s.ins CALL, :addr_0x542b, [30, 5000]
  s.ins MSGSET, uint(4701), byte(2), line(s, 4701)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_foreground_picture, [369, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 369
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4702), byte(1), line(s, 4702)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4703), byte(1), line(s, 4703)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4704), byte(1), line(s, 4704)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4705), byte(0), line(s, 4705)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_foreground_picture, [354, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 354
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(4706), byte(1), line(s, 4706)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 24, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(182)]
  s.ins MOV, byte(0), ushort(182), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4707), byte(127), line(s, 4707)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(13), 29
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [17, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(4708), byte(127), line(s, 4708)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 1
  s.ins MOV, byte(0), ushort(R_transition_duration), 200
  s.ins MOV, byte(0), ushort(13), 47
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [18, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(4709), byte(127), line(s, 4709)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [218]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4710), byte(2), line(s, 4710)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4711), byte(0), line(s, 4711)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4712), byte(1), line(s, 4712)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4713), byte(127), line(s, 4713)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4714), byte(127), line(s, 4714)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4715), byte(1), line(s, 4715)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x8e6ef
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
