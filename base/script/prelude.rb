require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_prelude(s)
  s.label :addr_0x78af
  s.ins JUMP, :addr_0x78b9
  s.label :addr_0x78b4
  s.ins JUMP, :addr_0x78b9
  s.label :addr_0x78b9
  s.ins MOV, byte(0), ushort(R_fuuka_zokusei_count), 0
  s.ins MOV, byte(0), ushort(R_karin_zokusei_count), 0
  s.ins MOV, byte(0), ushort(R_mina_zokusei_count), 0
  s.ins MOV, byte(0), ushort(R_doremi_zokusei_count), 0
  s.ins MOV, byte(0), ushort(R_fuuka_criminal_count), 0
  s.ins MOV, byte(0), ushort(R_karin_criminal_count), 0
  s.ins MOV, byte(0), ushort(R_mina_criminal_count), 0
  s.ins MOV, byte(0), ushort(R_doremi_criminal_count), 0
  s.ins MOV, byte(0), ushort(R_interrupt_A_count), 0
  s.ins MOV, byte(0), ushort(R_interrupt_B_count), 0
  s.ins MOV, byte(0), ushort(R_fuuka_role), 0
  s.ins MOV, byte(0), ushort(R_karin_role), 0
  s.ins MOV, byte(0), ushort(R_mina_role), 0
  s.ins MOV, byte(0), ushort(R_doremi_role), 0
  s.ins MOV, byte(0), ushort(R_route_type), 0
  s.ins MOV, byte(0), ushort(R_route_index), 0
  s.ins MOV, byte(0), ushort(178), 0
  s.ins JUMP, :addr_0x7913
  s.label :addr_0x7913
  s.ins CALC, ushort(R_fuuka_zokusei_count), byte(0), Register.new(138), byte(0), Register.new(139), byte(1), byte(0), Register.new(141), byte(1), byte(255)
  s.ins CALC, ushort(R_karin_zokusei_count), byte(0), Register.new(142), byte(0), Register.new(143), byte(1), byte(0), Register.new(145), byte(1), byte(255)
  s.ins CALC, ushort(R_mina_zokusei_count), byte(0), Register.new(144), byte(0), Register.new(146), byte(1), byte(0), Register.new(149), byte(1), byte(255)
  s.ins CALC, ushort(R_doremi_zokusei_count), byte(0), Register.new(140), byte(0), Register.new(147), byte(1), byte(0), Register.new(148), byte(1), byte(255)
  s.ins CALC, ushort(R_fuuka_criminal_count), byte(0), Register.new(138), byte(0), Register.new(139), byte(1), byte(0), Register.new(140), byte(1), byte(255)
  s.ins CALC, ushort(R_karin_criminal_count), byte(0), Register.new(141), byte(0), Register.new(142), byte(1), byte(0), Register.new(143), byte(1), byte(255)
  s.ins CALC, ushort(R_mina_criminal_count), byte(0), Register.new(144), byte(0), Register.new(145), byte(1), byte(0), Register.new(146), byte(1), byte(255)
  s.ins CALC, ushort(R_doremi_criminal_count), byte(0), Register.new(147), byte(0), Register.new(148), byte(1), byte(0), Register.new(149), byte(1), byte(255)
  s.ins CALC, ushort(R_interrupt_A_count), byte(0), Register.new(150), byte(0), Register.new(151), byte(1), byte(0), Register.new(152), byte(1), byte(0), Register.new(153), byte(1), byte(0), Register.new(154), byte(1), byte(0), Register.new(155), byte(1), byte(0), Register.new(156), byte(1), byte(0), Register.new(157), byte(1), byte(0), Register.new(158), byte(1), byte(0), Register.new(159), byte(1), byte(0), Register.new(160), byte(1), byte(0), Register.new(161), byte(1), byte(255)
  s.ins CALC, ushort(R_interrupt_B_count), byte(0), Register.new(154), byte(0), Register.new(155), byte(1), byte(0), Register.new(156), byte(1), byte(0), Register.new(157), byte(1), byte(0), Register.new(158), byte(1), byte(0), Register.new(159), byte(1), byte(0), Register.new(160), byte(1), byte(0), Register.new(161), byte(1), byte(255)
  s.ins DEBUG, '------------------------------------', byte(0)
  s.ins DEBUG, 'f.インタラプトＡ周回数 = %d', byte(1), Register.new(R_interrupt_A_count)
  s.ins DEBUG, 'f.インタラプトＢ周回数 = %d', byte(1), Register.new(R_interrupt_B_count)
  s.ins DEBUG, 'f.風華属性周回数 = %d', byte(1), Register.new(R_fuuka_zokusei_count)
  s.ins DEBUG, 'f.火凛属性周回数 = %d', byte(1), Register.new(R_karin_zokusei_count)
  s.ins DEBUG, 'f.水無属性周回数 = %d', byte(1), Register.new(R_mina_zokusei_count)
  s.ins DEBUG, 'f.土麗美属性周回数 = %d', byte(1), Register.new(R_doremi_zokusei_count)
  s.ins DEBUG, 'f.風華死刑囚周回数 = %d', byte(1), Register.new(R_fuuka_criminal_count)
  s.ins DEBUG, 'f.火凛死刑囚周回数 = %d', byte(1), Register.new(R_karin_criminal_count)
  s.ins DEBUG, 'f.水無死刑囚周回数 = %d', byte(1), Register.new(R_mina_criminal_count)
  s.ins DEBUG, 'f.土麗美死刑囚周回数 = %d', byte(1), Register.new(R_doremi_criminal_count)
  s.ins JUMP_TABLE, Register.new(R_interrupt_A_count), ushort(12), :addr_0x7b6f, :addr_0x7b83, :addr_0x7b97, :addr_0x7bab, :addr_0x7bbf, :addr_0x7bd3, :addr_0x7be7, :addr_0x7bfb, :addr_0x7c0f, :addr_0x7c23, :addr_0x7c39, :addr_0x7c4f
  s.label :addr_0x7b6f
  s.ins SAVEINFO, byte(0), 'First game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7b83
  s.ins SAVEINFO, byte(0), 'Second game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7b97
  s.ins SAVEINFO, byte(0), 'Third game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7bab
  s.ins SAVEINFO, byte(0), 'Fourth game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7bbf
  s.ins SAVEINFO, byte(0), 'Fifth game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7bd3
  s.ins SAVEINFO, byte(0), 'Sixth game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7be7
  s.ins SAVEINFO, byte(0), 'Seventh game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7bfb
  s.ins SAVEINFO, byte(0), 'Eighth game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7c0f
  s.ins SAVEINFO, byte(0), 'Ninth game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7c23
  s.ins SAVEINFO, byte(0), '10th game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7c39
  s.ins SAVEINFO, byte(0), '11th game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7c4f
  s.ins SAVEINFO, byte(0), '12th game'
  s.ins JUMP, :addr_0x7c65
  s.label :addr_0x7c65
  s.ins JUMP, :addr_0x7c6a
  s.label :addr_0x7c6a
  s.ins JUMP_COND, byte(0), Register.new(R_interrupt_A_count), 5, :addr_0x7c81
  s.ins JUMP_COND, byte(0), Register.new(R_interrupt_A_count), 6, :addr_0x7cf8
  s.ins JUMP, :card_select
  s.label :addr_0x7c81
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_fuuka_zokusei_count), byte(0), Register.new(R_karin_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7d21
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_fuuka_zokusei_count), byte(0), Register.new(R_mina_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7d42
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_fuuka_zokusei_count), byte(0), Register.new(R_doremi_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7d63
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_karin_zokusei_count), byte(0), Register.new(R_mina_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7d84
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_karin_zokusei_count), byte(0), Register.new(R_doremi_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7da5
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_mina_zokusei_count), byte(0), Register.new(R_doremi_zokusei_count), byte(1), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x7dc6
  s.ins JUMP, :card_select
  s.label :addr_0x7cf8
  s.ins JUMP_COND, byte(0), Register.new(R_fuuka_zokusei_count), 0, :addr_0x7de7
  s.ins JUMP_COND, byte(0), Register.new(R_karin_zokusei_count), 0, :addr_0x7dfc
  s.ins JUMP_COND, byte(0), Register.new(R_mina_zokusei_count), 0, :addr_0x7e11
  s.ins JUMP_COND, byte(0), Register.new(R_doremi_zokusei_count), 0, :addr_0x7e26
  s.ins JUMP, :card_select
  s.label :addr_0x7d21
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :fuuka_karin_setup, :fuuka_mina_setup, :karin_fuuka_setup, :karin_mina_setup, :karin_doremi_setup, :mina_karin_setup
  s.label :addr_0x7d42
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :fuuka_karin_setup, :fuuka_mina_setup, :karin_fuuka_setup, :mina_fuuka_setup, :mina_doremi_setup, :doremi_mina_setup
  s.label :addr_0x7d63
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :fuuka_karin_setup, :fuuka_mina_setup, :karin_fuuka_setup, :fuuka_doremi_setup, :doremi_fuuka_setup, :doremi_karin_setup
  s.label :addr_0x7d84
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :karin_mina_setup, :karin_doremi_setup, :mina_karin_setup, :mina_fuuka_setup, :mina_doremi_setup, :doremi_mina_setup
  s.label :addr_0x7da5
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :karin_mina_setup, :karin_doremi_setup, :mina_karin_setup, :fuuka_doremi_setup, :doremi_fuuka_setup, :doremi_karin_setup
  s.label :addr_0x7dc6
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(5)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(6), :mina_fuuka_setup, :mina_doremi_setup, :doremi_mina_setup, :fuuka_doremi_setup, :doremi_fuuka_setup, :doremi_karin_setup
  s.label :addr_0x7de7
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(2)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :fuuka_karin_setup, :fuuka_mina_setup, :karin_fuuka_setup
  s.label :addr_0x7dfc
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(2)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :karin_mina_setup, :karin_doremi_setup, :mina_karin_setup
  s.label :addr_0x7e11
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(2)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :mina_fuuka_setup, :mina_doremi_setup, :doremi_mina_setup
  s.label :addr_0x7e26
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(2)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :fuuka_doremi_setup, :doremi_fuuka_setup, :doremi_karin_setup
  s.label :card_select
  s.ins SAVEINFO, byte(1), 'Card selection'
  s.ins AUTOSAVE
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins CALL, :f_init_graphics, []
  s.ins LAYERLOAD, 0, byte(2), 0, byte(1), 38
  s.ins MOV, byte(0), ushort(R_temp), 0
  s.ins JUMP_COND, byte(0), Register.new(90), 0, :addr_0x7e78
  s.ins MOV, byte(8), ushort(R_temp), 1
  s.label :addr_0x7e78
  s.ins JUMP_COND, byte(0), Register.new(91), 0, :addr_0x7e86
  s.ins MOV, byte(8), ushort(R_temp), 2
  s.label :addr_0x7e86
  s.ins JUMP_COND, byte(0), Register.new(92), 0, :addr_0x7e94
  s.ins MOV, byte(8), ushort(R_temp), 4
  s.label :addr_0x7e94
  s.ins JUMP_COND, byte(0), Register.new(93), 0, :addr_0x7ea2
  s.ins MOV, byte(8), ushort(R_temp), 8
  s.label :addr_0x7ea2
  s.ins JUMP_COND, byte(0), Register.new(94), 0, :addr_0x7eb0
  s.ins MOV, byte(8), ushort(R_temp), 16
  s.label :addr_0x7eb0
  s.ins JUMP_COND, byte(0), Register.new(95), 0, :addr_0x7ebe
  s.ins MOV, byte(8), ushort(R_temp), 32
  s.label :addr_0x7ebe
  s.ins JUMP_COND, byte(0), Register.new(96), 0, :addr_0x7ecd
  s.ins MOV, byte(8), ushort(R_temp), 64
  s.label :addr_0x7ecd
  s.ins JUMP_COND, byte(0), Register.new(97), 0, :addr_0x7edc
  s.ins MOV, byte(8), ushort(R_temp), 128
  s.label :addr_0x7edc
  s.ins JUMP_COND, byte(0), Register.new(98), 0, :addr_0x7eeb
  s.ins MOV, byte(8), ushort(R_temp), 256
  s.label :addr_0x7eeb
  s.ins JUMP_COND, byte(0), Register.new(99), 0, :addr_0x7efa
  s.ins MOV, byte(8), ushort(R_temp), 512
  s.label :addr_0x7efa
  s.ins JUMP_COND, byte(0), Register.new(100), 0, :addr_0x7f09
  s.ins MOV, byte(8), ushort(R_temp), 1024
  s.label :addr_0x7f09
  s.ins JUMP_COND, byte(0), Register.new(101), 0, :addr_0x7f19
  s.ins MOV, byte(8), ushort(R_temp), 2048
  s.label :addr_0x7f19
  s.ins LAYERLOAD, 1, byte(9), 0, 2, Register.new(R_temp)
  s.ins UNARY, -1534, 0, Register.new(R_temp)
  s.ins TABLEGET, ushort(R_temp), Register.new(R_temp), ushort(12), 4, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0), 3, byte(0), byte(0), byte(0), 3, byte(0), byte(0), byte(0), 3, byte(0), byte(0), byte(0), 10, byte(0), byte(0), byte(0), 10, byte(0), byte(0), byte(0), 10, byte(0), byte(0), byte(0)
  s.ins CALL, :f_bgm_play, [Register.new(R_temp), 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins 0xe0, 1, 0, 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(12), :fuuka_karin_setup, :fuuka_mina_setup, :fuuka_doremi_setup, :karin_fuuka_setup, :karin_mina_setup, :karin_doremi_setup, :mina_fuuka_setup, :mina_karin_setup, :mina_doremi_setup, :doremi_fuuka_setup, :doremi_karin_setup, :doremi_mina_setup
  s.label :fuuka_karin_setup
  s.ins MOV, byte(0), ushort(R_route_index), 1
  s.ins MOV, byte(0), ushort(R_fuuka_role), 1
  s.ins MOV, byte(0), ushort(R_karin_role), 2
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 1
  s.ins SAVEINFO, byte(1), 'Condemned: Fuuka, Joker: Karin'
  s.ins JUMP, :jump_to_route
  s.label :fuuka_mina_setup
  s.ins MOV, byte(0), ushort(R_route_index), 2
  s.ins MOV, byte(0), ushort(R_fuuka_role), 1
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 2
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 1
  s.ins SAVEINFO, byte(1), 'Condemned: Fuuka, Joker: Mina'
  s.ins JUMP, :jump_to_route
  s.label :fuuka_doremi_setup
  s.ins MOV, byte(0), ushort(R_route_index), 3
  s.ins MOV, byte(0), ushort(R_fuuka_role), 1
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 2
  s.ins MOV, byte(0), ushort(R_route_type), 4
  s.ins SAVEINFO, byte(1), 'Condemned: Fuuka, Joker: Doremi'
  s.ins JUMP, :jump_to_route
  s.label :karin_fuuka_setup
  s.ins MOV, byte(0), ushort(R_route_index), 4
  s.ins MOV, byte(0), ushort(R_fuuka_role), 2
  s.ins MOV, byte(0), ushort(R_karin_role), 1
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 1
  s.ins SAVEINFO, byte(1), 'Condemned: Karin, Joker: Fuuka'
  s.ins JUMP, :jump_to_route
  s.label :karin_mina_setup
  s.ins MOV, byte(0), ushort(R_route_index), 5
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 1
  s.ins MOV, byte(0), ushort(R_mina_role), 2
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 2
  s.ins SAVEINFO, byte(1), 'Condemned: Karin, Joker: Mina'
  s.ins JUMP, :jump_to_route
  s.label :karin_doremi_setup
  s.ins MOV, byte(0), ushort(R_route_index), 6
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 1
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 2
  s.ins MOV, byte(0), ushort(R_route_type), 2
  s.ins SAVEINFO, byte(1), 'Condemned: Karin, Joker: Doremi'
  s.ins JUMP, :jump_to_route
  s.label :mina_fuuka_setup
  s.ins MOV, byte(0), ushort(R_route_index), 7
  s.ins MOV, byte(0), ushort(R_fuuka_role), 2
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 1
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 3
  s.ins SAVEINFO, byte(1), 'Condemned: Mina, Joker: Fuuka'
  s.ins JUMP, :jump_to_route
  s.label :mina_karin_setup
  s.ins MOV, byte(0), ushort(R_route_index), 8
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 2
  s.ins MOV, byte(0), ushort(R_mina_role), 1
  s.ins MOV, byte(0), ushort(R_doremi_role), 3
  s.ins MOV, byte(0), ushort(R_route_type), 2
  s.ins SAVEINFO, byte(1), 'Condemned: Mina, Joker: Karin'
  s.ins JUMP, :jump_to_route
  s.label :mina_doremi_setup
  s.ins MOV, byte(0), ushort(R_route_index), 9
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 1
  s.ins MOV, byte(0), ushort(R_doremi_role), 2
  s.ins MOV, byte(0), ushort(R_route_type), 3
  s.ins SAVEINFO, byte(1), 'Condemned: Mina, Joker: Doremi'
  s.ins JUMP, :jump_to_route
  s.label :doremi_fuuka_setup
  s.ins MOV, byte(0), ushort(R_route_index), 10
  s.ins MOV, byte(0), ushort(R_fuuka_role), 2
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 1
  s.ins MOV, byte(0), ushort(R_route_type), 4
  s.ins SAVEINFO, byte(1), 'Condemned: Doremi, Joker: Fuuka'
  s.ins JUMP, :jump_to_route
  s.label :doremi_karin_setup
  s.ins MOV, byte(0), ushort(R_route_index), 11
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 2
  s.ins MOV, byte(0), ushort(R_mina_role), 3
  s.ins MOV, byte(0), ushort(R_doremi_role), 1
  s.ins MOV, byte(0), ushort(R_route_type), 4
  s.ins SAVEINFO, byte(1), 'Condemned: Doremi, Joker: Karin'
  s.ins JUMP, :jump_to_route
  s.label :doremi_mina_setup
  s.ins MOV, byte(0), ushort(R_route_index), 12
  s.ins MOV, byte(0), ushort(R_fuuka_role), 3
  s.ins MOV, byte(0), ushort(R_karin_role), 3
  s.ins MOV, byte(0), ushort(R_mina_role), 2
  s.ins MOV, byte(0), ushort(R_doremi_role), 1
  s.ins MOV, byte(0), ushort(R_route_type), 3
  s.ins SAVEINFO, byte(1), 'Condemned: Doremi, Joker: Mina'
  s.ins JUMP, :jump_to_route
  s.label :jump_to_route
  s.ins BGMSTOP, 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 0, byte(0)
  s.ins LAYERUNLOAD, 1, byte(0)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 60
  s.ins AUTOSAVE
  s.ins JUMP, :addr_0x82ef
  s.label :addr_0x82ef
  s.ins FIND_IN_LIST, ushort(R_temp), 1, 0, ushort(4), Register.new(R_fuuka_role), byte(0), byte(0), Register.new(R_karin_role), byte(0), byte(0), Register.new(R_mina_role), byte(0), byte(0), Register.new(R_doremi_role), byte(0), byte(0)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(4), :addr_0x831f, :addr_0x833b, :addr_0x8357, :addr_0x8373
  s.ins JUMP, :addr_0x838f
  s.label :addr_0x831f
  s.ins JUMP_TABLE, Register.new(R_fuuka_criminal_count), ushort(2), :addr_0x8331, :addr_0x8336
  s.ins JUMP, :addr_0x838f
  s.label :addr_0x8331
  s.ins JUMP, :game_02_intA01_01_風華、初めての死刑囚
  s.label :addr_0x8336
  s.ins JUMP, :game_02_intA01_05_風華、２回目の死刑囚
  s.label :addr_0x833b
  s.ins JUMP_TABLE, Register.new(R_karin_criminal_count), ushort(2), :addr_0x834d, :addr_0x8352
  s.ins JUMP, :addr_0x838f
  s.label :addr_0x834d
  s.ins JUMP, :game_02_intA01_02_火凛、初めての死刑囚
  s.label :addr_0x8352
  s.ins JUMP, :game_02_intA01_06_火凛、２回目の死刑囚
  s.label :addr_0x8357
  s.ins JUMP_TABLE, Register.new(R_mina_criminal_count), ushort(2), :addr_0x8369, :addr_0x836e
  s.ins JUMP, :addr_0x838f
  s.label :addr_0x8369
  s.ins JUMP, :game_02_intA01_03_水無、初めての死刑囚
  s.label :addr_0x836e
  s.ins JUMP, :game_02_intA01_07_水無、２回目の死刑囚
  s.label :addr_0x8373
  s.ins JUMP_TABLE, Register.new(R_doremi_criminal_count), ushort(2), :addr_0x8385, :addr_0x838a
  s.ins JUMP, :addr_0x838f
  s.label :addr_0x8385
  s.ins JUMP, :game_02_intA01_04_土麗美、初めての死刑囚
  s.label :addr_0x838a
  s.ins JUMP, :game_02_intA01_08_土麗美、２回目の死刑囚
  s.label :addr_0x838f
  s.ins JUMP, :addr_0x8394
  s.label :addr_0x8394
  s.ins JUMP_TABLE, Register.new(R_interrupt_A_count), ushort(11), :addr_0x83ca, :addr_0x83cf, :addr_0x83d4, :addr_0x83d9, :addr_0x83de, :addr_0x83e3, :addr_0x83e8, :addr_0x83ed, :addr_0x83f2, :addr_0x83f7, :addr_0x83fc
  s.ins JUMP, :addr_0x8401
  s.label :addr_0x83ca
  s.ins JUMP, :game_02_intA02_01_初めての、ゲーム開始
  s.label :addr_0x83cf
  s.ins JUMP, :game_02_intA02_02_初めての、ゲーム開始２
  s.label :addr_0x83d4
  s.ins JUMP, :game_02_intA02_03_初めての、ゲーム開始３
  s.label :addr_0x83d9
  s.ins JUMP, :game_02_intA02_04_初めての、ゲーム開始４
  s.label :addr_0x83de
  s.ins JUMP, :game_02_intA02_05_初めての、ゲーム開始５
  s.label :addr_0x83e3
  s.ins JUMP, :game_02_intA02_06_初めての、ゲーム開始６
  s.label :addr_0x83e8
  s.ins JUMP, :game_02_intA02_07_初めての、ゲーム開始７
  s.label :addr_0x83ed
  s.ins JUMP, :game_02_intA02_08_初めての、ゲーム開始８
  s.label :addr_0x83f2
  s.ins JUMP, :game_02_intA02_09_初めての、ゲーム開始９
  s.label :addr_0x83f7
  s.ins JUMP, :game_02_intA02_10_初めての、ゲーム開始１０
  s.label :addr_0x83fc
  s.ins JUMP, :game_02_intA02_11_初めての、ゲーム開始１１
  s.label :addr_0x8401
  s.ins JUMP, :game_02_intA02_12_初めての、ゲーム開始１２
  s.label :addr_0x8406
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_index), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(11), :addr_0x8442, :addr_0x8447, :addr_0x844c, :addr_0x8451, :addr_0x8456, :addr_0x845b, :addr_0x8460, :addr_0x8465, :addr_0x846a, :addr_0x846f, :addr_0x8474
  s.ins JUMP, :addr_0x8479
  s.label :addr_0x8442
  s.ins JUMP, :game_03_gameA_01（死：風、ピ：火）風華属性
  s.label :addr_0x8447
  s.ins JUMP, :game_03_gameA_02（死：風、ピ：水）風華属性
  s.label :addr_0x844c
  s.ins JUMP, :game_03_gameA_03（死：風、ピ：土）土麗美属性
  s.label :addr_0x8451
  s.ins JUMP, :game_03_gameA_04（死：火、ピ：風）風華属性
  s.label :addr_0x8456
  s.ins JUMP, :game_03_gameA_05（死：火、ピ：水）火凛属性
  s.label :addr_0x845b
  s.ins JUMP, :game_03_gameA_06（死：火、ピ：土）火凛属性
  s.label :addr_0x8460
  s.ins JUMP, :game_03_gameA_07（死：水、ピ：風）水無属性
  s.label :addr_0x8465
  s.ins JUMP, :game_03_gameA_08（死：水、ピ：火）火凛属性
  s.label :addr_0x846a
  s.ins JUMP, :game_03_gameA_09（死：水、ピ：土）水無属性
  s.label :addr_0x846f
  s.ins JUMP, :game_03_gameA_10（死：土、ピ：風）土麗美属性
  s.label :addr_0x8474
  s.ins JUMP, :game_03_gameA_11（死：土、ピ：火）土麗美属性
  s.label :addr_0x8479
  s.ins JUMP, :game_03_gameA_12（死：土、ピ：水）水無属性
  s.label :addr_0x847e
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_type), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(4), :addr_0x849e, :addr_0x8507, :addr_0x8570, :addr_0x85d9
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x849e
  s.ins FIND_IN_LIST, ushort(R_temp), Register.new(R_route_index), 0, ushort(3), 1, byte(0), byte(0), byte(0), 2, byte(0), byte(0), byte(0), 4, byte(0), byte(0), byte(0)
  s.ins JUMP_TABLE, Register.new(R_fuuka_zokusei_count), ushort(3), :addr_0x84c8, :addr_0x84dd, :addr_0x84f2
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x84c8
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_fuuka_1, :game_04_event_fuuka_2, :game_04_event_fuuka_3
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x84dd
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_fuuka_4, :game_04_event_fuuka_5, :game_04_event_fuuka_6
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x84f2
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_fuuka_7, :game_04_event_fuuka_8, :game_04_event_fuuka_9
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8507
  s.ins FIND_IN_LIST, ushort(R_temp), Register.new(R_route_index), 0, ushort(3), 5, byte(0), byte(0), byte(0), 6, byte(0), byte(0), byte(0), 8, byte(0), byte(0), byte(0)
  s.ins JUMP_TABLE, Register.new(R_karin_zokusei_count), ushort(3), :addr_0x8531, :addr_0x8546, :addr_0x855b
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8531
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_karin_1, :game_04_event_karin_2, :game_04_event_karin_3
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8546
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_karin_4, :game_04_event_karin_5, :game_04_event_karin_6
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x855b
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_karin_7, :game_04_event_karin_8, :game_04_event_karin_9
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8570
  s.ins FIND_IN_LIST, ushort(R_temp), Register.new(R_route_index), 0, ushort(3), 7, byte(0), byte(0), byte(0), 9, byte(0), byte(0), byte(0), 12, byte(0), byte(0), byte(0)
  s.ins JUMP_TABLE, Register.new(R_mina_zokusei_count), ushort(3), :addr_0x859a, :addr_0x85af, :addr_0x85c4
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x859a
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_mina_1, :game_04_event_mina_2, :game_04_event_mina_3
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x85af
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_mina_4, :game_04_event_mina_5, :game_04_event_mina_6
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x85c4
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_mina_7, :game_04_event_mina_8, :game_04_event_mina_9
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x85d9
  s.ins FIND_IN_LIST, ushort(R_temp), Register.new(R_route_index), 0, ushort(3), 3, byte(0), byte(0), byte(0), 10, byte(0), byte(0), byte(0), 11, byte(0), byte(0), byte(0)
  s.ins JUMP_TABLE, Register.new(R_doremi_zokusei_count), ushort(3), :addr_0x8603, :addr_0x8618, :addr_0x862d
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8603
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_doremi_1, :game_04_event_doremi_2, :game_04_event_doremi_3
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8618
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_doremi_4, :game_04_event_doremi_5, :game_04_event_doremi_6
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x862d
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(3), :game_04_event_doremi_7, :game_04_event_doremi_8, :game_04_event_doremi_9
  s.ins JUMP, :addr_0x8642
  s.label :addr_0x8642
  s.ins JUMP, :addr_0x5f4bf
  s.label :addr_0x8647
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_index), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(12), :addr_0x8682, :addr_0x8693, :addr_0x86a4, :addr_0x86b5, :addr_0x86c6, :addr_0x86d7, :addr_0x86e8, :addr_0x86f9, :addr_0x870a, :addr_0x871b, :addr_0x872c, :addr_0x873d
  s.label :addr_0x8682
  s.ins JUMP_TABLE, Register.new(R_fuuka_zokusei_count), ushort(3), :game_05_gameB前半01, :addr_0x625a7, :addr_0x626bf
  s.label :addr_0x8693
  s.ins JUMP_TABLE, Register.new(R_fuuka_zokusei_count), ushort(3), :game_05_gameB前半02, :addr_0x653ce, :addr_0x65425
  s.label :addr_0x86a4
  s.ins JUMP_TABLE, Register.new(R_doremi_zokusei_count), ushort(3), :game_05_gameB前半03, :addr_0x69258, :addr_0x693f9
  s.label :addr_0x86b5
  s.ins JUMP_TABLE, Register.new(R_fuuka_zokusei_count), ushort(3), :game_05_gameB前半04, :addr_0x6b526, :addr_0x6b59f
  s.label :addr_0x86c6
  s.ins JUMP_TABLE, Register.new(R_karin_zokusei_count), ushort(3), :game_05_gameB前半05, :addr_0x6f5f2, :addr_0x6f706
  s.label :addr_0x86d7
  s.ins JUMP_TABLE, Register.new(R_karin_zokusei_count), ushort(3), :game_05_gameB前半06, :addr_0x732e5, :addr_0x733fd
  s.label :addr_0x86e8
  s.ins JUMP_TABLE, Register.new(R_mina_zokusei_count), ushort(3), :game_05_gameB前半07, :addr_0x78002, :addr_0x780b5
  s.label :addr_0x86f9
  s.ins JUMP_TABLE, Register.new(R_karin_zokusei_count), ushort(3), :game_05_gameB前半08, :addr_0x7b76e, :addr_0x7b898
  s.label :addr_0x870a
  s.ins JUMP_TABLE, Register.new(R_mina_zokusei_count), ushort(3), :game_05_gameB前半09, :addr_0x7eb8c, :addr_0x7ec3d
  s.label :addr_0x871b
  s.ins JUMP_TABLE, Register.new(R_doremi_zokusei_count), ushort(3), :game_05_gameB前半10, :addr_0x81067, :addr_0x811e6
  s.label :addr_0x872c
  s.ins JUMP_TABLE, Register.new(R_doremi_zokusei_count), ushort(3), :game_05_gameB前半11, :addr_0x85cef, :addr_0x85e27
  s.label :addr_0x873d
  s.ins JUMP_TABLE, Register.new(R_mina_zokusei_count), ushort(3), :game_05_gameB前半12, :addr_0x89c3b, :addr_0x89d38
  s.label :addr_0x874e
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_type), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(4), :addr_0x876e, :addr_0x877c, :addr_0x878a, :addr_0x8798
  s.ins JUMP, :game_06_intB01_02_初めての火凛イベント時
  s.label :addr_0x876e
  s.ins JUMP_COND, byte(1), Register.new(R_fuuka_zokusei_count), 0, :addr_0x87a6
  s.ins JUMP, :game_06_intB01_01_初めての風華イベント時
  s.label :addr_0x877c
  s.ins JUMP_COND, byte(1), Register.new(R_karin_zokusei_count), 0, :addr_0x87a6
  s.ins JUMP, :game_06_intB01_02_初めての火凛イベント時
  s.label :addr_0x878a
  s.ins JUMP_COND, byte(1), Register.new(R_mina_zokusei_count), 0, :addr_0x87a6
  s.ins JUMP, :game_06_intB01_03_初めての水無イベント時
  s.label :addr_0x8798
  s.ins JUMP_COND, byte(1), Register.new(R_doremi_zokusei_count), 0, :addr_0x87a6
  s.ins JUMP, :game_06_intB01_04_初めての土麗美イベント時
  s.label :addr_0x87a6
  s.ins JUMP_TABLE, Register.new(R_interrupt_B_count), ushort(7), :addr_0x87cc, :addr_0x87d1, :addr_0x87d6, :addr_0x87db, :addr_0x87e0, :addr_0x87e5, :addr_0x87ea
  s.ins JUMP, :addr_0x87ef
  s.label :addr_0x87cc
  s.ins JUMP, :game_06_intB02_01_それ以外時のイベント（１回目）
  s.label :addr_0x87d1
  s.ins JUMP, :game_06_intB02_02_それ以外時のイベント（２回目）
  s.label :addr_0x87d6
  s.ins JUMP, :game_06_intB02_03_それ以外時のイベント（３回目）
  s.label :addr_0x87db
  s.ins JUMP, :game_06_intB02_04_それ以外時のイベント（４回目）
  s.label :addr_0x87e0
  s.ins JUMP, :game_06_intB02_05_それ以外時のイベント（５回目）
  s.label :addr_0x87e5
  s.ins JUMP, :game_06_intB02_06_それ以外時のイベント（６回目）
  s.label :addr_0x87ea
  s.ins JUMP, :game_06_intB02_07_それ以外時のイベント（７回目）
  s.label :addr_0x87ef
  s.ins JUMP, :game_06_intB02_08_それ以外時のイベント（８回目）
  s.label :addr_0x87f4
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_index), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(11), :addr_0x8830, :addr_0x8835, :addr_0x883a, :addr_0x883f, :addr_0x8844, :addr_0x8849, :addr_0x884e, :addr_0x8853, :addr_0x8858, :addr_0x885d, :addr_0x8862
  s.ins JUMP, :addr_0x8867
  s.label :addr_0x8830
  s.ins JUMP, :game_07_gameB後半01
  s.label :addr_0x8835
  s.ins JUMP, :game_07_gameB後半02
  s.label :addr_0x883a
  s.ins JUMP, :game_07_gameB後半03
  s.label :addr_0x883f
  s.ins JUMP, :game_07_gameB後半04
  s.label :addr_0x8844
  s.ins JUMP, :game_07_gameB後半05
  s.label :addr_0x8849
  s.ins JUMP, :game_07_gameB後半06
  s.label :addr_0x884e
  s.ins JUMP, :game_07_gameB後半07
  s.label :addr_0x8853
  s.ins JUMP, :game_07_gameB後半08
  s.label :addr_0x8858
  s.ins JUMP, :game_07_gameB後半09
  s.label :addr_0x885d
  s.ins JUMP, :game_07_gameB後半10
  s.label :addr_0x8862
  s.ins JUMP, :game_07_gameB後半11
  s.label :addr_0x8867
  s.ins JUMP, :game_07_gameB後半12
  s.label :addr_0x886c
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_type), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(4), :addr_0x888c, :addr_0x889a, :addr_0x88a8, :addr_0x88b6
  s.ins JUMP, :game_08_intZ01_01_初めての風華イベント時
  s.label :addr_0x888c
  s.ins JUMP_COND, byte(1), Register.new(R_fuuka_zokusei_count), 0, :addr_0x88c4
  s.ins JUMP, :game_08_intZ01_01_初めての風華イベント時
  s.label :addr_0x889a
  s.ins JUMP_COND, byte(1), Register.new(R_karin_zokusei_count), 0, :addr_0x88c4
  s.ins JUMP, :game_08_intZ01_02_初めての火凛イベント時
  s.label :addr_0x88a8
  s.ins JUMP_COND, byte(1), Register.new(R_mina_zokusei_count), 0, :addr_0x88c4
  s.ins JUMP, :game_08_intZ01_03_初めての水無イベント時
  s.label :addr_0x88b6
  s.ins JUMP_COND, byte(1), Register.new(R_doremi_zokusei_count), 0, :addr_0x88c4
  s.ins JUMP, :game_08_intZ01_04_初めての土麗美イベント時
  s.label :addr_0x88c4
  s.ins JUMP_TABLE, Register.new(R_interrupt_B_count), ushort(7), :game_08_intZ02_01_その他（１度目）, :game_08_intZ02_02_その他（２度目）, :game_08_intZ02_03_その他（３度目）, :game_08_intZ02_04_その他（４度目）, :game_08_intZ02_05_その他（５度目）, :game_08_intZ02_06_その他（６度目）, :game_08_intZ02_07_その他（７度目）
  s.ins JUMP, :addr_0xad131
  s.ins JUMP, :game_08_intZ02_01_その他（１度目）
  s.ins JUMP, :game_08_intZ02_02_その他（２度目）
  s.ins JUMP, :game_08_intZ02_03_その他（３度目）
  s.ins JUMP, :game_08_intZ02_04_その他（４度目）
  s.ins JUMP, :game_08_intZ02_05_その他（５度目）
  s.ins JUMP, :game_08_intZ02_06_その他（６度目）
  s.ins JUMP, :game_08_intZ02_07_その他（７度目）
  s.ins JUMP, :addr_0xad131
  s.label :addr_0x8912
  s.ins MSGCLOSE, byte(0)
  s.ins WAIT, byte(0), 180
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 120
  s.ins MOV, byte(130), ushort(R_temp), Register.new(R_interrupt_A_count), 2
  s.ins TROPHY, byte(177)
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_route_index), byte(0), 1, byte(2), byte(0), 14, byte(1), byte(255)
  s.ins TROPHY, byte(177)
  s.ins JUMP, :addr_0x8980
  s.label :addr_0x8980
  s.ins JUMP_TABLE, Register.new(R_interrupt_A_count), ushort(13), :addr_0x89b9, :addr_0x89c3, :addr_0x89ce, :addr_0x89d9, :addr_0x89e4, :addr_0x89ef, :addr_0x89fa, :addr_0x8a05, :addr_0x8a10, :addr_0x8a1b, :addr_0x8a26, :addr_0x8a31, :addr_0x8bfe
  s.label :addr_0x89b9
  s.ins MOV, byte(0), ushort(2), 0
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89c3
  s.ins MOV, byte(0), ushort(2), 83
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89ce
  s.ins MOV, byte(0), ushort(2), 166
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89d9
  s.ins MOV, byte(0), ushort(2), 250
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89e4
  s.ins MOV, byte(0), ushort(2), 333
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89ef
  s.ins MOV, byte(0), ushort(2), 416
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x89fa
  s.ins MOV, byte(0), ushort(2), 500
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x8a05
  s.ins MOV, byte(0), ushort(2), 583
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x8a10
  s.ins MOV, byte(0), ushort(2), 666
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x8a1b
  s.ins MOV, byte(0), ushort(2), 750
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x8a26
  s.ins MOV, byte(0), ushort(2), 833
  s.ins JUMP, :addr_0x8a3c
  s.label :addr_0x8a31
  s.ins MOV, byte(0), ushort(2), 916
  s.ins JUMP, :addr_0x8b36
  s.label :addr_0x8a3c
  s.ins CALL, :f_se_play, [3, 35, 100, 1000, 0]
  s.ins WAIT, byte(0), 120
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [3, 4]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERLOAD, 10, byte(2), 0, byte(1), 522
  s.ins LAYERCTRL, 10, 5, byte(1), 1800
  s.ins LAYERLOAD, 12, byte(2), 0, byte(1), 525
  s.ins LAYERCTRL, 12, 5, byte(1), 1600
  s.ins LAYERLOAD, 11, byte(2), 0, byte(1), 524
  s.ins LAYERCTRL, 11, 5, byte(1), 1700
  s.ins LAYERCTRL, 11, 18, byte(1), Register.new(2)
  s.ins LAYERLOAD, 13, byte(2), 0, byte(1), 523
  s.ins LAYERCTRL, 13, 5, byte(1), 1500
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 120
  s.ins SEONCE, 124, 0, 1000, 0, 1000
  s.ins MOV, byte(2), ushort(2), 83
  s.ins LAYERCTRL, 11, 18, byte(3), Register.new(2), 60
  s.ins LAYERWAIT, 11, byte(1), 18
  s.ins WAIT, byte(1), 180
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins LAYERUNLOAD, 10, byte(0)
  s.ins LAYERUNLOAD, 12, byte(0)
  s.ins LAYERUNLOAD, 11, byte(0)
  s.ins LAYERUNLOAD, 13, byte(0)
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 60
  s.ins JUMP, :addr_0x78b4
  s.label :addr_0x8b36
  s.ins CALL, :f_se_play, [3, 35, 100, 1000, 0]
  s.ins WAIT, byte(0), 120
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [3, 4]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERLOAD, 10, byte(2), 0, byte(1), 522
  s.ins LAYERCTRL, 10, 5, byte(1), 1800
  s.ins LAYERLOAD, 12, byte(2), 0, byte(1), 525
  s.ins LAYERCTRL, 12, 5, byte(1), 1600
  s.ins LAYERLOAD, 11, byte(2), 0, byte(1), 524
  s.ins LAYERCTRL, 11, 5, byte(1), 1700
  s.ins LAYERCTRL, 11, 18, byte(1), Register.new(2)
  s.ins LAYERLOAD, 13, byte(2), 0, byte(1), 523
  s.ins LAYERCTRL, 13, 5, byte(1), 1500
  s.ins MOV, byte(0), ushort(R_transition_mode), 0
  s.ins MOV, byte(0), ushort(R_transition_duration), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 132
  s.ins SEONCE, 124, 0, 1000, 0, 1000
  s.ins LAYERCTRL, 12, 18, byte(7), -999, 360, 128
  s.ins MOV, byte(2), ushort(2), 83
  s.ins LAYERCTRL, 11, 18, byte(3), Register.new(2), 360
  s.ins LAYERWAIT, 11, byte(1), 18
  s.ins CALL, :f_se_play, [0, 36, 100, 0, 1]
  s.ins WAIT, byte(1), 60
  s.ins JUMP, :game_08_intZ02_08_その他（８度目）
  s.label :addr_0x8bfe
  s.ins EXIT
  s.ins EXIT
  s.ins EXIT
end
