Translations and other source files of an English patch for 幻想牢獄のカレイドスコープ.

- Translation: 神子
- Editing: Amarena, Neurochitin
- Graphics and technical implementation: Neurochitin

Goes together with https://gitlab.com/Neurochitin/kaleido.

# ⚠ Unfinished ⚠

This project is not yet finished! The translation is complete but there is still some polish to be done before making a public release. If you cannot wait, you can follow the compilation steps listed below to compile the patch yourself.

# Compilation

**Note for Windows users:** This repository uses symbolic links. Either follow [these instructions](https://stackoverflow.com/a/59761201) before cloning this repository, or move `font/varela/varela.fnt` to `rom_repack/newrodin.fnt` (replacing the existing file) after cloning. Also, it is unclear if it is possible to compile EnterExtractor for Windows natively, although there should be no problems in WSL.

You will need:

- Ruby
- ImageMagick
- [Kaleido](https://gitlab.com/Neurochitin/kaleido), branch `miko_kaleido`, cloned locally
- [EnterExtractor](https://github.com/07th-mod/enter_extractor)
- The `data.rom` of 幻想牢獄のカレイドスコープ extracted to a folder tree, for example using EnterExtractor's `blabla.py`, or using [sdu](https://github.com/DCNick3/shin#what-else-is-in-the-box) (`sdu rom extract path/to/data.rom path/to/output_folder`)

Set the constants `KALEIDO_PATH` and `EXTRACT_PATH` in `compile.rb` to the respective folder paths on your system, and `EE_PATH` to the compiled EnterExtractor executable. Then, run

```
ruby compile.rb path/to/patch.rom
```

which will compile the scripts to a `patch.rom` at the given path. Then, copy the `patch.rom` into a suitable RomFS patch folder of your desired Switch emulator or CFW.
